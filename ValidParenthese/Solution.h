#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <stack>
using namespace std;

class Solution {
public:
    bool isValid(string s) {
		if (s.length() == 0) {
			return true;
		}

        stack<char> strCheck;

		strCheck.push(s[0]);
		int len = s.length();
		for (int i = 1; i < len; i++) {
			if ((s[i] == ')' && !strCheck.empty() && strCheck.top() == '(') || 
				(s[i] == '}' && !strCheck.empty() && strCheck.top() == '{') ||
				(s[i] == ']' && !strCheck.empty() && strCheck.top() == '[')) {
				strCheck.pop();
			} else if (s[i] == '(' || s[i] == '{' || s[i] == '[') {
				strCheck.push(s[i]);
			} else {
				strCheck.push(s[i]);
				break;
			}
		}

		if (strCheck.empty()) {
			return true;
		}

		return false;
    }
};

#endif