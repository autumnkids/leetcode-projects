#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <stack>
#include <limits>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    int minDepth(TreeNode *root) {
		if (root == NULL) {
			return 0;
		}

        int depth = 0;
		int minDepth = INT_MAX;
		TreeNode *prevNode = NULL;
		TreeNode *curNode = NULL;
		stack<TreeNode *> dfs;
		dfs.push(root);
		while (!dfs.empty()) {
			curNode = dfs.top();
			if (prevNode == NULL || prevNode->left == curNode || prevNode->right == curNode) {
				depth++;
				if (curNode->left) {
					dfs.push(curNode->left);
				} else if (curNode->right) {
					dfs.push(curNode->right);
				} else {
					dfs.pop();
					if (depth < minDepth) {
						minDepth = depth;
					}
					depth--;
				}
			} else if (curNode->left == prevNode) {
				if (curNode->right) {
					dfs.push(curNode->right);
				}
			} else {
				dfs.pop();
				depth--;
			}

			prevNode = curNode;
		}

		return minDepth;
    }
};

#endif