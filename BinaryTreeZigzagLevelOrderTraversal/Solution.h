#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <queue>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    vector<vector<int> > zigzagLevelOrder(TreeNode *root) {
        vector<vector<int> > result;
		if (root == NULL) {
			return result;
		}

		vector<int> output;
		BFS(result, output, root);
		return result;
    }

private:
	void BFS(vector<vector<int> > &result, vector<int> &output, TreeNode *root) {
		bool oddLvl = true;
		queue<TreeNode *> bfs;
		bfs.push(root);
		int curLvlCount = 1;
		int nextLvlCount = 0;

		while (!bfs.empty()) {
			TreeNode *curNode = bfs.front();
			bfs.pop();
			curLvlCount--;

			if (curNode->left) {
				bfs.push(curNode->left);
				nextLvlCount++;
			}
			if (curNode->right) {
				bfs.push(curNode->right);
				nextLvlCount++;
			}

			if (oddLvl) {
				output.push_back(curNode->val);
			} else {
				output.insert(output.begin(), curNode->val);
			}

			if (curLvlCount == 0) {
				curLvlCount = nextLvlCount;
				nextLvlCount = 0;
				result.push_back(output);
				output.clear();
				oddLvl = !oddLvl;
			}
		}
	}
};

#endif