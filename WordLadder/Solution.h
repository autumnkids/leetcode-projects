#ifndef _SOULTION_H
#define _SOULTION_H

#include <unordered_set>
#include <queue>
#include <string>
using namespace std;

class Solution {
public:
    int ladderLength(string start, string end, unordered_set<string> &dict) {
		if ((start.empty() || end.empty()) || (start.length() != start.length())) {
			return 0;
		}
		
		int dist = 0;
		BFS(start, end, dist, dict);
		return dist;
    }

private:
	void BFS(string &start, string &end, int &dist, unordered_set<string> &dict) {
		unordered_set<string> explored;
		queue<string> qBFS;
		qBFS.push(start);
		int curLvlChild = 1;
		int nextLvlChild = 0;
		int lvl = 1;

		while (!qBFS.empty()) {
			string front = qBFS.front();
			qBFS.pop();
			curLvlChild--;
			if (front.compare(end) == 0) {
				dist = lvl;
				break;
			}

			int slen = front.length();
			for (int i = 0; i < slen; i++) {
				string tmpStr = front;
				tmpStr[i] = (tmpStr[i] - 'a' + 2) % 26 == 0 ? 'z' : (tmpStr[i] - 'a' + 2) % 26 + 'a' - 1;
				while (tmpStr[i] != front[i]) {
					if ((dict.find(tmpStr) != dict.end() || tmpStr.compare(end) == 0) && explored.find(tmpStr) == explored.end()) {
						qBFS.push(tmpStr);
						explored.insert(tmpStr);
						nextLvlChild++;
					}
					tmpStr[i] = (tmpStr[i] - 'a' + 2) % 26 == 0 ? 'z' : (tmpStr[i] - 'a' + 2) % 26 + 'a' - 1;
				}
				if ((dict.find(tmpStr) != dict.end() || tmpStr.compare(end) == 0) && explored.find(tmpStr) == explored.end()) {
					qBFS.push(tmpStr);
					explored.insert(tmpStr);
					nextLvlChild++;
				}
			}

			if (curLvlChild == 0) {
				lvl++;
				curLvlChild = nextLvlChild;
				nextLvlChild = 0;
			}
		}
	}
};

#endif