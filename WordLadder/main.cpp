#include <iostream>
#include "Solution.h"

int main() {
	Solution sol;
	string start("a");
	string end("c");
	unordered_set<string> dict;
	// dict.insert("hot"); dict.insert("dot"); dict.insert("dog"); dict.insert("lot"); dict.insert("log");
	dict.insert("a"); dict.insert("b"); dict.insert("c");
	cout << sol.ladderLength(start, end, dict) << endl;

	system("pause");
	return 0;
}