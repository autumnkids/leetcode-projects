#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

class Solution {
public:
    vector<int> plusOne(vector<int> &digits) {
		vector<int> result;
		if (digits.empty()) {
			return result;
		}

		int numSize = digits.size();
		int aheadDigit = 1;
		for (int i = numSize - 1; i >= 0; i--) {
			digits[i] = digits[i] + aheadDigit;
			aheadDigit = digits[i] / 10;
			digits[i] %= 10;
		}
		if (aheadDigit > 0) {
			result.push_back(aheadDigit);
		}
		for (int i = 0; i < numSize; i++) {
			result.push_back(digits[i]);
		}

		return result;
    }
};

#endif