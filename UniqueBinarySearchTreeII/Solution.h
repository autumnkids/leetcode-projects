#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    vector<TreeNode *> generateTrees(int n) {
        return BuildTree(1, n);
    }

private:
	vector<TreeNode *> BuildTree(int left, int right) {
		vector<TreeNode *> result;
		if (left > right) {
			result.push_back(NULL);
		} else {
			for (int i = left; i <= right; i++) {
				vector<TreeNode *> leftTree = BuildTree(left, i - 1);
				vector<TreeNode *> rightTree = BuildTree(i + 1, right);
				int lSize = leftTree.size();
				int rSize = rightTree.size();
				for (int j = 0; j < lSize; j++) {
					for (int k = 0; k < rSize; k++) {
						TreeNode *node = new TreeNode(i);
						node->left = leftTree[j];
						node->right = rightTree[k];
						result.push_back(node);
					}
				}
			}
		}

		return result;
	}
};

#endif