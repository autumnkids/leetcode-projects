#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
#include <vector>
#include <algorithm>
using namespace std;

class Solution {
public:
	string multiply(string num1, string num2) {
		if (num1.empty() || num2.empty()) {
			return "";
		}

		reverse(num1.begin(), num1.end());
		reverse(num2.begin(), num2.end());
		int nlen1 = num1.length();
		int nlen2 = num2.length();
		string ret(nlen1 + nlen2 + 1, '0');
		for (int i = 0; i < nlen2; i++) {
			int multiplier = num2[i] - '0';
			int carry = 0;
			for (int j = 0; j < nlen1; j++) {
				int multiplicand = num1[j] - '0';
				int tmp_res = multiplier * multiplicand + carry + (ret[i + j] - '0');
				ret[i + j] = tmp_res % 10 + '0';
				carry = tmp_res / 10;
			}
			if (carry != 0) {
				ret[i + nlen1] = carry + '0';
			}
		}

		int zeroes = 0;
		reverse(ret.begin(), ret.end());
		int retlen = ret.length();
		while (zeroes < retlen && ret[zeroes] == '0') {
			zeroes++;
		}
		if (zeroes == retlen) {
			return "0";
		}
		return ret.substr(zeroes, retlen - zeroes + 1);
	}

	/*
    string multiply(string num1, string num2) {
        string ret;
		if (num1.empty() || num2.empty()) {
			return ret;
		}

		int sign = 1;
		if (num1[0] == '-') {
			sign *= -1;
			num1.erase(num1.begin(), num1.begin() + 1);
		}
		if (num2[0] == '-') {
			sign *= -1;
			num2.erase(num2.begin(), num2.begin() + 1);
		}
		int nlen1 = num1.length();
		int nlen2 = num2.length();
		int curDigit = 0;
		vector<int> result;
		for (int i = nlen2 - 1; i >= 0; i--) {
			vector<int> tmp;
			int multiplier = num2[i] - '0';
			int tmp_carry = 0;
			for (int j = nlen1 - 1; j >= 0; j--) {
				int multiplicand = num1[j] - '0';
				int curRes = multiplicand * multiplier + tmp_carry;
				tmp.insert(tmp.begin(), curRes % 10);
				tmp_carry = curRes / 10;
			}
			if (tmp_carry != 0) {
				tmp.insert(tmp.begin(), tmp_carry);
			}
			if (result.empty()) {
				result.assign(tmp.begin(), tmp.end());
			} else {
				int carry = 0;
				int resSize = result.size();
				int k = tmp.size() - 1;
				for (int j = resSize - curDigit - 1; j >= 0; j--, k--) {
					int plus_res = result[j] + tmp[k] + carry;
					carry = plus_res / 10;
					result[j] = plus_res % 10;
				}
				while (k >= 0) {
					int plus_res = tmp[k] + carry;
					carry = plus_res / 10;
					result.insert(result.begin(), plus_res % 10);
					k--;
				}
				if (carry != 0) {
					result.insert(result.begin(), carry);
				}
			}
			curDigit++;
		}

		if (sign == -1) {
			ret = ret + "-";
		}
		int retSize = result.size();
		int zeroes = 0;
		while (zeroes < retSize && result[zeroes] == 0) {
			zeroes++;
		}
		if (zeroes == retSize) {
			zeroes--;
		}
		result.erase(result.begin(), result.begin() + zeroes);
		retSize = result.size();
		for (int i = 0; i < retSize; i++) {
			ret.append(1, result[i] + '0');
		}
		return ret;
    }
	*/
};

#endif