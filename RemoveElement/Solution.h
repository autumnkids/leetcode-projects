#ifndef _SOLUTION_H
#define _SOLUTION_H

class Solution {
public:
    int removeElement(int A[], int n, int elem) {
		if (n == 0) {
			return 0;
		}

        int i = 0, j = 0; // i keep increasing, j is the new length
		while (i < n && j < n) {
			if (A[i] == elem) {
				// Find the next element that is not the target
				while (i < n && A[i] == elem) {
					i++;
				}
				if (i < n) { // Replace j with i
					A[j] = A[i];
					j++;
				}
			} else if (j != i) {
				A[j] = A[i];
				j++;
			} else if (j == i) {
				j++;
			}
			i++;
		}

		return j;
    }
};

#endif