#ifndef _SOLUTION_H_
#define _SOLUTION_H_

#include <iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *removeNthFromEnd(ListNode *head, int n) {
		if (head == NULL || (head->next == NULL && n == 1)) {
			return NULL;
		}

		ListNode *lastNode = head; // Will reach to the last node first
        int i = 1;

		while (i < n) {
			lastNode = lastNode->next;
			i++;
		}

		// Find the last N - 1
		ListNode *firstNode = NULL;
		while (lastNode->next) {
			lastNode = lastNode->next;
			if (firstNode == NULL) {
				firstNode = head;
			} else {
				firstNode = firstNode->next;
			}
		}

		// Remove the node
		ListNode *removeNode = firstNode == NULL ? head : firstNode->next;
		ListNode *nextNode = removeNode->next;
		if (firstNode == NULL) {
			removeNode->next = NULL;
			delete removeNode;
			return nextNode;
		}
		firstNode->next = nextNode;
		removeNode->next = NULL;
		delete removeNode;

		return head;
    }
};

#endif