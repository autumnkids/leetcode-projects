#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
using namespace std;

class Solution {
public:
	string longestPalindrome(string s) {
		if (s.empty()) {
			return s;
		}
		
		string t = PreProcess(s);
		int tlen = t.length();
		int C = 0, R = 0;
		int *P = new int[tlen];
		for (int i = 1; i < tlen - 1; i++) { // Start from '^'
			int i_mirror = C - (i - C);

			P[i] = (R > i) ? Min(R - i, P[i_mirror]) : 0;
			// Expand P[i]
			while (t[i + 1 + P[i]] == t[i - 1 - P[i]]) {
				P[i]++;
			}

			// If palindrome centered at i expand past R,
			// adjust center based on explanded palindrome
			if (i + P[i] > R) {
				C = i;
				R = i + P[i];
			}
		}

		int maxLen = 0;
		int centerIdx = 0;
		for (int i = 1; i < tlen - 1; i++) {
			if (P[i] > maxLen) {
				maxLen = P[i];
				centerIdx = i;
			}
		}
		delete [] P;

		string ret(s, (centerIdx - 1 - maxLen) / 2, maxLen);
		return ret;
	}

private:
	inline int Min(int a, int b) {
		return (a < b) ? a : b;
	}

	string PreProcess(string s) {
		string t = "^";
		int slen = s.length();
		for (int i = 0; i < slen; i++) {
			t.push_back('#');
			t.push_back(s[i]);
		}
		t += "#$";
		return t;
	}
};

#endif