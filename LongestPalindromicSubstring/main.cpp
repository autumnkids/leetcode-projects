#include <iostream>
#include "Solution.h"

int main() {
	Solution sol;
	string str("abccbbccba");
	cout << sol.longestPalindrome(str) << endl;

	system("pause");

	return 0;
}