#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *insertionSortList(ListNode *head) {
		if (head == NULL) {
			return NULL;
		}

		ListNode *sorted = new ListNode(-1);
		sorted->next = head;
		ListNode *unsort = head->next;
		head->next = NULL;
		while (unsort != NULL) {
			ListNode *prevNode = sorted;
			ListNode *curNode = sorted->next;
			while (curNode != NULL) {
				if (curNode->val > unsort->val) {
					ListNode *temp = unsort;
					unsort = unsort->next;
					temp->next = curNode;
					prevNode->next = temp;
					break;
				} else {
					prevNode = curNode;
					curNode = curNode->next;
				}
			}
			if (curNode == NULL) {
				ListNode *temp = unsort;
				unsort = unsort->next;
				prevNode->next = temp;
				temp->next = NULL;
			}
		}
		
		ListNode *ret = sorted->next;
		delete sorted;
		return ret;
    }
};

#endif