#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

class Solution {
public:
	enum INPUT {
		SPACE,
		INVALID,
		NUM,
		DOT,
		SIGN,
		EXPONENT
	};

    bool isNumber(const char *s) {
		if (s == NULL) {
			return false;
		}

		// States
		int states[9][6] = {
			// SPC INV NUM DOT SIN EXP
			{	0,	-1,	1,	2,	8,	-1	}, // 0 Start input, or just space input
			{	7,	-1,	1,	3,	-1,	4	}, // 1 After input number
			{	-1,	-1,	3,	-1,	-1,	-1	}, // 2 After input dot, and none numbers beforehead
			{	7,	-1,	3,	-1,	-1,	4	}, // 3 After input numbers and dot
			{	-1,	-1,	6,	-1,	5,	-1	}, // 4 After input 'e' or 'E'
			{	-1,	-1,	6,	-1,	-1,	-1	}, // 5 After input 'e' and sign
			{	7,	-1,	6,	-1,	-1,	-1	}, // 6 After input 'e' + number
			{	7,	-1,	-1,	-1,	-1,	-1	}, // 7 After input valid numbers + space
			{	-1,	-1,	1,	2,	-1,	-1	}  // 8 After input sign
		};

		int state = 0;
		while ((* s) != '\0') {
			char cur = (* s);
			INPUT input = INVALID;
			if (cur == ' ') { input = SPACE; }
			else if (cur >= '0' && cur <= '9') { input = NUM; }
			else if (cur == '.') { input = DOT; }
			else if (cur == '-' || cur == '+') { input = SIGN; }
			else if (cur == 'e' || cur == 'E') { input = EXPONENT; }

			state = states[state][input];
			if (state == -1) { return false; }

			s++;
		}

		return state == 1 || state == 3 || state == 6 || state == 7;
    }
};

#endif