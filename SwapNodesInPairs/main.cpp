#include "Solution.h"

int main() {
	ListNode *head = new ListNode(1);
	head->next = new ListNode(2);
	head->next->next = new ListNode(3);
	head->next->next->next = new ListNode(4);
	Solution sol;
	sol.swapPairs(head);

	return 0;
}