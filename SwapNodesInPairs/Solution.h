#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *swapPairs(ListNode *head) {
		if (head == NULL) {
			return NULL;
		}

		ListNode *dummy = new ListNode(-1);
		dummy->next = head;
		ListNode *pre = dummy;
		ListNode *cur = head;
		ListNode *post = head->next;

		while (post != NULL) {
			ListNode *next = post->next;
			pre->next = post;
			post->next = cur;
			cur->next = next;

			pre = cur;
			cur = next;
			if (cur == NULL) {
				break;
			}
			post = next->next;
		}

		head = dummy->next;
		delete dummy;
		return head;
    }
};

#endif