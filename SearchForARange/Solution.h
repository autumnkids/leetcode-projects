#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

class Solution {
public:
    vector<int> searchRange(int A[], int n, int target) {
		vector<int> result;
		if (n == 0) {
			result.push_back(-1); result.push_back(-1);
			return result;
		}

		int istart = INT_MAX;
		int iend = -1;
		BinarySearch(A, target, 0, n - 1, istart, iend);
		if (istart == INT_MAX || iend == -1) {
			result.push_back(-1); result.push_back(-1);
			return result;
		}
		result.push_back(istart); result.push_back(iend);
		return result;
    }

private:
	void BinarySearch(int A[], int target, int start, int end, int &istart, int &iend) {
		if (start > end) {
			return ;
		}

		int mid = (start + end) / 2;
		if (A[mid] == target) {
			if (mid < istart) {
				istart = mid;
			}
			if (mid > iend) {
				iend = mid;
			}
		}
		BinarySearch(A, target, start, mid - 1, istart, iend);
		BinarySearch(A, target, mid + 1, end, istart, iend);
	}
};

#endif