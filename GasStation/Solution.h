#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

class Solution {
public:
    int canCompleteCircuit(vector<int> &gas, vector<int> &cost) {
		if (gas.empty() || cost.empty()) {
			return -1;
		}

		int nSize = gas.size();
		int *diff = new int[nSize];
		for (int i = 0; i < nSize; i++) {
			diff[i] = gas[i] - cost[i];
		}

		int leftGas = 0, sum = 0, startnode = -1;
		for (int i = 0; i < nSize; i++) {
			leftGas += diff[i];
			sum += diff[i];
			if (sum < 0) { // The start node should not be this
				startnode = i + 1;
				sum = 0;
			}
		}

		delete[] diff;

		if (leftGas < 0) {
			return -1;
		}

		return startnode;
    }
};

#endif