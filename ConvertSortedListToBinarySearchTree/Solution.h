#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    TreeNode *sortedListToBST(ListNode *head) {
		if (head == NULL) {
			return NULL;
		}

		int len = 0;
		ListNode *curListNode = head;
		while (curListNode != NULL) {
			curListNode = curListNode->next;
			len++;
		}
		
		TreeNode *root = new TreeNode(0);
		BuildTree(root, head, len);
		return root;
    }

private:
	void BuildTree(TreeNode *root, ListNode *head, int targetLen) {
		if (root == NULL) {
			return ;
		}

		ListNode *curListNode = head;
		int midIdx = targetLen / 2;
		for (int i = 0; i < midIdx; i++) {
			// Move to the middle node
			curListNode = curListNode->next;
		}
		root->val = curListNode->val;
		if (midIdx >= 1) {
			root->left = new TreeNode(0);
			BuildTree(root->left, head, midIdx);
		}
		if (targetLen - midIdx - 1 >= 1) {
			root->right = new TreeNode(0);
			BuildTree(root->right, curListNode->next, targetLen - midIdx - 1);
		}
	}
};

#endif