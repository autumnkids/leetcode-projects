#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
#include <algorithm>
#include <unordered_map>
using namespace std;

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
		int slen = s.length();
		if (slen == 0) {
			return slen;
		}

		int subLen = 1;
		int maxSubLen = 1;
		vector<char> record;
		record.push_back(s[0]);
		for (int i = 1; i < slen; i++) {
			vector<char>::iterator iter = find(record.begin(), record.end(), s[i]);
			if (iter != record.end()) {
				// Pop all elements <= iter
				int lenToDelete = iter - record.begin() + 1;
				record.erase(record.begin(), iter + 1);
				subLen -= lenToDelete;
			}
			record.push_back(s[i]);
			subLen++;

			if (subLen > maxSubLen) {
				maxSubLen = subLen;
			}
		}

		return maxSubLen;
    }
};

#endif