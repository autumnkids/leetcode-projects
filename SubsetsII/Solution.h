#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <algorithm>
#include <set>
using namespace std;

class Solution {
public:
    vector<vector<int> > subsetsWithDup(vector<int> &S) {
        vector<vector<int> > result;
		vector<int> output;
		set<vector<int> > explored;
		if (S.empty()) {
			return result;
		}

		sort(S.begin(), S.end());
		BackTracing(S, 0, output, result, explored);
		return result;
    }

private:
	void BackTracing(vector<int> &S, int start, vector<int> &output, vector<vector<int> > &result, set<vector<int> > &explored) {
		if (start >= S.size()) {
			if (explored.find(output) == explored.end()) {
				result.push_back(output);
				explored.insert(output);
			}
			return ;
		}

		int nSize = S.size();
		for (int i = start; i < nSize; i++) {
			output.push_back(S[i]);
			BackTracing(S, i + 1, output, result, explored);
			output.pop_back();
			if (explored.find(output) == explored.end()) {
				result.push_back(output);
				explored.insert(output);
			}
		}
	}
};

#endif