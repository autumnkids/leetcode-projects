#include "Solution.h"

int main() {
	UndirectedGraphNode *node = new UndirectedGraphNode(0);
	node->neighbors.push_back(node);
	node->neighbors.push_back(node);
	Solution sol;
	sol.cloneGraph(node);

	return 0;
}