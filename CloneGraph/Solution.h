#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <unordered_map>
#include <queue>
using namespace std;

struct UndirectedGraphNode {
    int label;
    vector<UndirectedGraphNode *> neighbors;
    UndirectedGraphNode(int x) : label(x) {};
};

class Solution {
public:
    UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node) {
		if (node == NULL) {
			return NULL;
		}


		UndirectedGraphNode *cloneNode = new UndirectedGraphNode(node->label);
		queue<UndirectedGraphNode *> visit;
		unordered_map<UndirectedGraphNode *, UndirectedGraphNode *> nodeMap;
		visit.push(node);
		nodeMap[node] = cloneNode;
		
		while (!visit.empty()) {
			UndirectedGraphNode *front = visit.front();
			visit.pop();

			int nSize = front->neighbors.size();
			for (int i = 0; i < nSize; i++) {
				if (nodeMap.find(front->neighbors[i]) == nodeMap.end()) {
					UndirectedGraphNode *cloneNeighbor = new UndirectedGraphNode(front->neighbors[i]->label);
					nodeMap[front]->neighbors.push_back(cloneNeighbor);
					visit.push(front->neighbors[i]);
					nodeMap[front->neighbors[i]] = cloneNeighbor;
				} else {
					nodeMap[front]->neighbors.push_back(nodeMap[front->neighbors[i]]);
				}
			}
		}

		return cloneNode;
    }
};

#endif