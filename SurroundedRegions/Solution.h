#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <queue>
using namespace std;

class Solution {
public:
	virtual ~Solution() {
		delete [] m_visit;
	}

    void solve(vector<vector<char>> &board) {
		if (board.empty()) {
			return ;
		}

		m_rowSize = board.size();
		m_colSize = board[0].size();
		m_visit = new bool*[m_rowSize];
		for (int i = 0; i < m_rowSize; i++) {
			m_visit[i] = new bool[m_colSize];
			memset(m_visit[i], 0, sizeof(bool) * m_colSize);
		}

		for (int i = 0; i < m_rowSize; i++) {
			for (int j = 0; j < m_colSize; j++) {
				if (board[i][j] == 'O' && !m_visit[i][j]) {
					BFS(board, i, j);
				}
			}
		}
    }

private:
	bool **m_visit;
	int m_rowSize;
	int m_colSize;
	struct Grid {
		int row, col;
	};

	void BFS(vector<vector<char>> &board, int srow, int scol) {
		queue<Grid> explore;
		vector<char *> found;
		bool needChange = true;
		int moves[][2] = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};
		Grid grid; grid.row = srow; grid.col = scol;
		explore.push(grid);

		while (!explore.empty()) {
			Grid front = explore.front();
			explore.pop();
			if (m_visit[front.row][front.col]) {
				continue;
			}
			found.push_back(&board[front.row][front.col]);
			m_visit[front.row][front.col] = true;

			for (int i = 0; i < 4; i++) {
				if (front.row + moves[i][0] >= 0 && front.col + moves[i][1] >= 0 && front.row + moves[i][0] < m_rowSize && front.col + moves[i][1] < m_colSize) {
					int nextRow = front.row + moves[i][0];
					int nextCol = front.col + moves[i][1];
					if (!m_visit[nextRow][nextCol] && board[nextRow][nextCol] == 'O') {
						Grid nextGrid; nextGrid.row = nextRow; nextGrid.col = nextCol;
						explore.push(nextGrid);
					}
				} else { // Reached the boundary
					// The 'O' should be the one on the boundary
					needChange = false;
				}
			}
		}

		if (needChange && !found.empty()) {
			for (int i = 0; i < found.size(); i++) {
				(* found[i]) = 'X';
			}
		}
	}
};

#endif