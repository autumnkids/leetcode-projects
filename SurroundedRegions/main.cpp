#include <iostream>
#include "Solution.h"

int main() {
	vector<vector<char>> board;
	vector<char> row;
	row.push_back('X'); row.push_back('X'); row.push_back('X');
	board.push_back(row);
	row.clear();
	row.push_back('X'); row.push_back('O'); row.push_back('X');
	board.push_back(row);
	row.clear();
	row.push_back('X'); row.push_back('X'); row.push_back('X');
	board.push_back(row);

	Solution sol;
	sol.solve(board);

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			cout << board[i][j];
		}
		cout << endl;
	}

	system("pause");

	return 0;
}