#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
		if (l1 == NULL) {
			return l2;
		} else if (l2 == NULL) {
			return l1;
		}

		ListNode *tmp1 = l1;
		ListNode *tmp2 = l2;
		ListNode *result = NULL;
		ListNode *final = NULL;
		if (tmp1->val < tmp2->val) {
			result = tmp1;
			tmp1 = tmp1->next;
		} else {
			result = tmp2;
			tmp2 = tmp2->next;
		}
		final = result;

		while (tmp1 && tmp2) {
			if (tmp1->val > tmp2->val) {
				result->next = tmp2;
				tmp2 = tmp2->next;
			} else {
				result->next = tmp1;
				tmp1 = tmp1->next;
			}
			result = result->next;
		}

		if (tmp1 != NULL) {
			result->next = tmp1;
		} else {
			result->next = tmp2;
		}

		return final;
    }
};

#endif