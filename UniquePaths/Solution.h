#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
#include <stack>
using namespace std;

class Solution {
public:
    int uniquePaths(int m, int n) {
		if (m == 0 || n == 0) {
			return 0;
		}

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					ways[i][j] = 1;
				} else if (j == 0) {
					ways[i][j] = 1;
				} else {
					ways[i][j] = ways[i - 1][j] + ways[i][j - 1];
				}
			}
		}

		return ways[m - 1][n - 1];
    }

private:
	int ways[100][100];
};

#endif