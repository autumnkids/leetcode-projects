#ifndef _SOLUTION_H_
#define _SOLUTION_H_

#include <iostream>
#include <string>
using namespace std;

class Solution {
public:
    string convert(string s, int nRows) {
		string result;
		if (nRows == 0 || s.empty()) {
			return result;
		} else if (nRows == 1) {
			return s;
		}

        int row = 1; // Current row
		int col = 1; // Current column
		int curIdx = 0; // Current idx in s

		for (int i = 1; i <= nRows; i++) {
		    row = i;
			curIdx = row - 1;

			if (row == nRows) { // It's the same as the first row
				row = 1;
			}

			col = 1;
			while (curIdx < s.length()) {
				result.append(1, s[curIdx]);

				if (col % 2 == 0 && row != 1) { // Middle rows
					curIdx += (row - 1) * 2;
				} else {
					curIdx += (nRows - row) * 2;
				}

				col++;
			}
		}

		return result;
    }
};

#endif