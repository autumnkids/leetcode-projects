#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <string>
#include <map>
using namespace std;

class Solution {
public:
	Solution() {
		string s("23456789");
		string sub[8] = {"abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
		for (int i = 0; i < 8; i++) {
			m_dict[s[i]] = sub[i];
		}
	}

	virtual ~Solution() {
		m_dict.clear();
	}

    vector<string> letterCombinations(string digits) {
        vector<string> result;
		string output;
		FindAllStrings(digits, 0, output, result);
		return result;
    }

private:
	map<char, string> m_dict;

	void FindAllStrings(string &digits, int curIdx, string &output, vector<string> &result) {
		if (digits.size() <= curIdx) {
			result.push_back(output);
			return ;
		}

		char firstCh = digits[curIdx];
		string sub = m_dict[firstCh];
		int slen = sub.length();
		for (int i = 0; i < slen; i++) {
			output.push_back(sub[i]);
			FindAllStrings(digits, curIdx + 1, output, result);
			output.pop_back();
		}
	}
};

#endif