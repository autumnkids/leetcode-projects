#include <iostream>
#include "Solution.h"

int main() {
	Solution sol;
	string s = "234";
	vector<string> result;
	result = sol.letterCombinations(s);

	int vSize = result.size();
	for (int i = 0; i < vSize; i++) {
		cout << result[i] << endl;
	}

	system("pause");
	return 0;
}