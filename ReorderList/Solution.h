#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <stack>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    void reorderList(ListNode *head) {
		if (head == NULL) {
			return;
		}

		int listLen = 0;
		ListNode *tmpNode = head;

		// Put nodes into the stack
		stack<ListNode *> listStack;
		tmpNode = head;
		while (tmpNode) {
			listStack.push(tmpNode);
			tmpNode = tmpNode->next;
		}
		listLen = listStack.size();

		// Start reconnect the list
		tmpNode = head;
		for (int i = 0; i < listLen >> 1; i++) {
			ListNode *topNode = listStack.top();
			listStack.pop();

			ListNode *nextNode = tmpNode->next;
			if (nextNode == topNode) {
				topNode->next = NULL;
				break;
			}
			tmpNode->next = topNode;
			topNode->next = nextNode;
			tmpNode = nextNode;
		}
		if (listLen % 2 != 0) {
			tmpNode->next = NULL;
		}
    }
};

#endif