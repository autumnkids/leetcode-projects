#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

class Solution {
public:
    int maxArea(vector<int> &height) {
		if (height.empty() || height.size() == 1) {
			return 0;
		}

		int left = 0;
		int right = height.size() - 1;
		int max_area = 0;
		while (left < right) {
			int min_height = min(height[left], height[right]);
			int area = min_height * (right - left);
			if (area > max_area) {
				max_area = area;
			}
			if (height[left] < height[right]) {
				left++;
			} else {
				right--;
			}
		}

		return max_area;
    }
};

#endif