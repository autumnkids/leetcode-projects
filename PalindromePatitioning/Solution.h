#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
#include <vector>
using namespace std;

class Solution {
public:
    vector<vector<string>> partition(string s) {
		vector<vector<string> > result;
		vector<string> output;
        Recurse(s, 0, output, result);

		return result;
    }

private:
	void Recurse(string &s, int start, vector<string> &output, vector<vector<string> > &result) {
		if (start == s.size()) {
			result.push_back(output);
			return;
		}

		for (int i = start; i < s.size(); i++) {
			if (IsPalindrome(s, start, i)) {
				output.push_back(s.substr(start, i - start + 1));
				Recurse(s, i + 1, output, result);
				output.pop_back();
			}
		}
	}

	bool IsPalindrome(string &s, int start, int end) {
		int mid = (start + end + 1) / 2;
		int i = 0, j = 0;
		for (i = start, j = end; i < mid; i++, j--) {
			if (s[i] != s[j]) {
				break;
			}
		}

		if (i < mid) {
			return false;
		}

		return true;
	}
};

#endif