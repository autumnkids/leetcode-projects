#ifndef _SOLUTION_H_
#define _SOLUTION_H_

#include <vector>

using namespace std;

class Solution {
public:
    Solution() {
        m_check = new bool[9];
    }
    
    virtual ~Solution() {
        delete [] m_check;
    }

    bool isValidSudoku(vector<vector<char> > &board) {
        // Check each row
		for (int row = 0; row < 9; row++) {
			InitialCheck();
			for (int col = 0; col < 9; col++) {
				if (!CheckOneCell(board, row, col)) {
					return false;
				}
			}
		}

		// Check each col
		for (int col = 0; col < 9; col++) {
			InitialCheck();
			for (int row = 0; row < 9; row++) {
				if (!CheckOneCell(board, row, col)) {
					return false;
				}
			}
		}

		// Check each backet
		for (int i = 0; i < 9; i += 3) { // Loop nine squares
			for (int j = 0; j < 9; j += 3) {
				InitialCheck();
				for (int row = 0; row < 3; row++) { // Loop inner nine grids
					for (int col = 0; col < 3; col++) {
						if (!CheckOneCell(board, i + row, j + col)) {
							return false;
						}
					}
				}
			}
		}

		return true;
    }
    
private:
    bool *m_check; // Array to check each row, column, grids

	void InitialCheck() {
		for (int i = 0; i < 9; i++) {
			m_check[i] = false;
		}
	}

	bool CheckOneCell(vector<vector<char> > &board, int row, int col) {
		if (board[row][col] != '.' && board[row][col] >= '1' && board[row][col] <= '9') {
			if (m_check[board[row][col] - '1']) {
				return false;
			} else {
				m_check[board[row][col] - '1'] = true;
			}
		}
		return true;
	} 
};

#endif