#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
#include <stack>
#include <algorithm>
using namespace std;

class Solution {
public:
    string simplifyPath(string path) {
		string ret = "";
		if (path.empty()) {
			return ret;
		}

		if (path[path.length() - 1] != '/') {
			path.push_back('/');
		}
		stack<string> str_parts;
		string tpath = path;
		while (!tpath.empty()) {
			size_t islash = tpath.find("/");
			string cur_part(tpath.begin(), tpath.begin() + islash);
			if (cur_part.empty()) {
				tpath = tpath.substr(islash + 1);
				continue;
			}
			if (cur_part == ".") {
				tpath = tpath.substr(islash + 1);
				continue;
			}
			if (cur_part == "..") {
				if (!str_parts.empty()) {
					str_parts.pop();
				}
			} else {
				str_parts.push(cur_part);
			}
			tpath = tpath.substr(islash + 1);
		}

		if (str_parts.empty()) {
			ret = "/";
		}
		while (!str_parts.empty()) {
			string top = str_parts.top();
			str_parts.pop();
			top = "/" + top;
			ret.insert(0, top);
		}
		return ret;
    }
};

#endif