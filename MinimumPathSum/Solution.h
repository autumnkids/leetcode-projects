#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

class Solution {
public:
    int minPathSum(vector<vector<int> > &grid) {
        int sum = 0;
		vector<vector<int> > grid_sum = grid;
		if (grid.empty()) {
			return sum;
		}

		int rSize = grid.size();
		int cSize = grid[0].size();
		for (int row = 1; row < rSize; row++) {
			grid_sum[row][0] = grid_sum[row][0] + grid_sum[row - 1][0];
		}
		for (int col = 1; col < cSize; col++) {
			grid_sum[0][col] = grid_sum[0][col] + grid_sum[0][col - 1];
		}
		for (int row = 1; row < rSize; row++) {
			for (int col = 1; col < cSize; col++) {
				grid_sum[row][col] = min(grid_sum[row - 1][col] + grid_sum[row][col], grid_sum[row][col - 1] + grid_sum[row][col]);
			}
		}

		return grid_sum[rSize - 1][cSize - 1];
    }
};

#endif