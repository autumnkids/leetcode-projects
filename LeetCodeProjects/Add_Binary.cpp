#include <iostream>
#include <string>
using namespace std;

class Solution {
public:
    string addBinary(string a, string b) {
		int resultLength = a.size() > b.size() ? a.size() : b.size();
		int carry = 0;
		int ch_a, ch_b;
		string result, retResult;
		resultLength++;
		int i, j;
		for (i = a.size() - 1, j = b.size() - 1; i >= 0 || j >= 0; i--, j--) {
			ch_a = (i >= 0 && a[i] == '1') ? 1 : 0;
			ch_b = (j >= 0 && b[j] == '1') ? 1 : 0;
			carry = ch_a + ch_b + carry;
			switch (carry) {
			case 3:
				result.push_back('1');
				carry = 1;
				break;
			case 2:
				result.push_back('0');
				carry = 1;
				break;
			case 1:
				result.push_back('1');
				carry = 0;
				break;
			case 0:
				result.push_back('0');
				carry = 0;
				break;
			}
		}
		if (carry == 1) {
			result.push_back('1');
		}
		for (int i = result.size() - 1; i >= 0; i--) {
			retResult.push_back(result[i]);
		}
		return retResult;
    }
};
/*
int main() {
	string stra, strb, re;
	while (1) {
		cin >> stra;
		cin >> strb;
		Solution sol;
		re = sol.addBinary(stra, strb);
		cout << re << endl;
	}
	return 0;
}*/