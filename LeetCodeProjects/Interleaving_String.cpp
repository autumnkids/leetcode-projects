#include <iostream>
#include <string>
using namespace std;

class Solution {
public:
    bool isInterleave(string s1, string s2, string s3) {
		int len1 = s1.length();
		int len2 = s2.length();
		int len3 = s3.length();

		if (len1 + len2 != len3)
			return false;

		char curr, next;
		bool **graph = new bool *[len3];
		for (int i = 0; i < len3; i++) {
			graph[i] = new bool[len3];
			memset(graph[i], 0, sizeof(graph[i]));
		}
		for (int i = 0; i < len3 - 1; i++) {
			curr = s3[i];
			for (int j = i + 1; j < len3; j++) {
				next = s3[j];
				for (int k = 0; k < len1 - 1; k++) {
					if (s1[k] == curr && s1[k + 1] == next && graph[k][k + 1] == false) {
						graph[k][k + 1] = true;
					}
				}
				for (int k = 0; k < len2 - 1; k++) {
					if (s2[k] == curr && s2[k + 1] == next && graph[k + len1][k + len1 + 1] == false) {
						graph[k + len1][k + len1 + 1] = true;
					}
				}
			}
		}
		bool result = true;
		for (int i = 0; i < len1 - 1; i++) {
			if (graph[i][i + 1] == false) {
				result = false;
			}
		}
		for (int i = 0; i < len2 - 1; i++) {
			if (graph[i + len1][i + len1 + 1] == false) {
				result = false;
			}
		}
		return result;
    }
};

int main() {
	string str1, str2, str3;
	Solution sol;
	bool result;
	while (1) {
		cin >> str1;
		cin >> str2;
		cin >> str3;
		result = sol.isInterleave(str1, str2, str3);
		cout << result << endl;
	}
	return 0;
}