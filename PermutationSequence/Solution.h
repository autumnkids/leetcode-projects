#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
#include <vector>
using namespace std;

class Solution {
public:
    string getPermutation(int n, int k) {
		if (n == 0 || k == 0) {
			return "";
		}

		vector<int> nums(n);
		int permCount = 1;
		for (int i = 0; i < n; i++) {
			nums[i] = i + 1;
			permCount *= (i + 1);
		}

		k--;
		string ret;
		for (int i = 0; i < n; i++) {
			permCount /= (n - i);
			int idx = k / permCount;
			ret.push_back(nums[idx] + '0');
			nums.erase(nums.begin() + idx);
			k = k % permCount;
		}

		return ret;
    }
};

#endif