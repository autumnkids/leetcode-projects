#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    vector<vector<int> > pathSum(TreeNode *root, int sum) {
        vector<vector<int> > result;
		if (root == NULL) {
			return result;
		}

		DFS(root, sum, result);
		return result;
    }

private:
	void DFS(TreeNode *root, int sum, vector<vector<int> > &result) {
		stack<TreeNode *> preOrder;
		preOrder.push(root);
		TreeNode *prevNode = NULL;
		TreeNode *curNode = NULL;

		int tmpSum = 0;
		vector<int> output;
		while (!preOrder.empty()) {
			curNode = preOrder.top();

			if (prevNode == NULL || prevNode->left == curNode || prevNode->right == curNode) {
				tmpSum += curNode->val;
				output.push_back(curNode->val);
				if (curNode->left) {
					preOrder.push(curNode->left);
				} else if (curNode->right) {
					preOrder.push(curNode->right);
				} else {
					preOrder.pop();
					if (sum == tmpSum) {
						result.push_back(output);
					}
					output.pop_back();
					tmpSum -= curNode->val;
				}
			} else if (curNode->left == prevNode) {
				if (curNode->right) {
					preOrder.push(curNode->right);
				}
			} else {
				preOrder.pop();
				output.pop_back();
				tmpSum -= curNode->val;
			}

			prevNode = curNode;
		}
	}
};

#endif