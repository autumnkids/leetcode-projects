#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <set>
#include <algorithm>
using namespace std;

class Solution {
public:
    vector<vector<int> > threeSum(vector<int> &num) {
	    vector<vector<int> >result;
		if (num.empty() || num.size() < 3) {
			return result;
		}

		sort(num.begin(), num.end());
		for (int i = 0; i < num.size(); i++) {
			int start = i + 1;
			int end = num.size() - 1;
			while (start < end) {
				if (num[i] + num[start] + num[end] == 0) {
					vector<int> output;
					output.push_back(num[i]);
					output.push_back(num[start]);
					output.push_back(num[end]);
					result.push_back(output);

					start++; end--;
					while (start < end && num[start] == num[start - 1]) { start++; }
					while (start < end && num[end] == num[end + 1]) { end--; }
				} else if (num[i] + num[start] + num[end] < 0) {
					start++;
				} else {
					end--;
				}
			}
			if (i < num.size() - 1) {
				while (num[i] == num[i + 1]) {
					i++;
				}
			}
		}

		return result;
    }
};

#endif