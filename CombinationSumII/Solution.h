#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <algorithm>
#include <set>
using namespace std;

class Solution {
public:
    vector<vector<int> > combinationSum2(vector<int> &num, int target) {
        vector<vector<int> > result;
		vector<int> output;
		set<vector<int> > dict;
		if (num.empty()) {
			return result;
		}
		sort(num.begin(), num.end());
		BackTracing(result, output, num, dict, target, 0, 0);
		return result;
    }

private:
	void BackTracing(vector<vector<int> > &result, vector<int> &output, vector<int> &num, set<vector<int> > &dict, int target, int curIdx, int sum) {
		if (sum == target) {
			if (dict.find(output) == dict.end()) {
				result.push_back(output);
				dict.insert(output);
			}
			return ;
		}
		if (sum > target) {
			return ;
		}

		int nSize = num.size();
		for (int i = curIdx; i < nSize; i++) {
			sum += num[i];
			output.push_back(num[i]);
			BackTracing(result, output, num, dict, target, i + 1, sum);
			output.pop_back();
			sum -= num[i];
			while (i < nSize && num[i] == num[i + 1]) { i++; }
		}
	}
};

#endif