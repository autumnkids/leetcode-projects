#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class BSTIterator {
public:
    BSTIterator(TreeNode *root) {
		m_iterator = root;

		if (m_iterator != NULL) {
			m_midOrder.push(root);
			while (m_iterator->left) {
				m_midOrder.push(m_iterator->left);
				m_iterator = m_iterator->left;
			}
		}
    }

    /** @return whether we have a next smallest number */
    bool hasNext() {
		if (m_midOrder.empty()) {
			return false;
		}

		return true;
    }

    /** @return the next smallest number */
    int next() {
		int retv = 0;
		m_iterator = m_midOrder.top();
		m_midOrder.pop();
		retv = m_iterator->val;
		if (m_iterator->right) {
			m_midOrder.push(m_iterator->right);
			m_iterator = m_iterator->right;
			while (m_iterator->left) {
				m_midOrder.push(m_iterator->left);
				m_iterator = m_iterator->left;
			}
		}

		return retv;
    }

private:
	TreeNode *m_iterator;
	stack<TreeNode *> m_midOrder;
};

/**
 * Your BSTIterator will be called like this:
 * BSTIterator i = BSTIterator(root);
 * while (i.hasNext()) cout << i.next();
 */

#endif