#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    bool hasPathSum(TreeNode *root, int sum) {
		if (root == NULL) {
			return false;
		}

		int curSum = 0;
        stack<TreeNode *> dfs;
		dfs.push(root);
		TreeNode *prevNode = NULL;
		TreeNode *curNode = NULL;
		while (!dfs.empty()) {
			curNode = dfs.top();
			if (prevNode == NULL || prevNode->left == curNode || prevNode->right == curNode) {
				curSum += curNode->val;
				if (curNode->left) {
					dfs.push(curNode->left);
				} else if (curNode->right) {
					dfs.push(curNode->right);
				} else {
					dfs.pop();
					if (curSum != sum) {
						curSum -= curNode->val;
					} else {
						return true;
					}
				}
			} else if (curNode->left == prevNode) {
				if (curNode->right) {
					dfs.push(curNode->right);
				}
			} else {
				dfs.pop();
				curSum -= curNode->val;
			}

			prevNode = curNode;
		}

		return false;
    }
};

#endif