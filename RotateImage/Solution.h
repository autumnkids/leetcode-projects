#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <algorithm>
using namespace std;

class Solution {
public:
	// Note:
	//		- First, swap along the diagonal
	//		- Second, swap up and down
    void rotate(vector<vector<int> > &matrix) {
		int nSize = matrix.size();
		if (nSize == 0) { return ; }

		// Swap diagonal
		for (int i = 0; i < nSize - 1; i++) {
			for (int j = 0; j < nSize - i - 1; j++) {
				swap(matrix[i][j], matrix[nSize - j - 1][nSize - i - 1]);
			}
		}

		// Swap up and down
		for (int i = 0; i < nSize / 2; i++) {
			for (int j = 0; j < nSize; j++) {
				swap(matrix[i][j], matrix[nSize - i - 1][j]);
			}
		}
    }
};

#endif