#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
#include <vector>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    TreeNode *sortedArrayToBST(vector<int> &num) {
		if (num.empty()) {
			return NULL;
		}

		TreeNode *root = new TreeNode(0);
		BuildBST(root, num, 0, num.size() - 1);
		return root;
    }

private:
	void BuildBST(TreeNode *root, vector<int> &num, int start, int end) {
		if (root == NULL) {
			return ;
		}

		int midIdx = (end - start + 1) / 2 + start;
		root->val = num[midIdx];
		if (midIdx - start >= 1) {
			root->left = new TreeNode(0);
			BuildBST(root->left, num, start, midIdx - 1);
		}
		if (end - midIdx >= 1) {
			root->right = new TreeNode(0);
			BuildBST(root->right, num, midIdx + 1, end);
		}
	}
};

#endif