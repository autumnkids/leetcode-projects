#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    int sumNumbers(TreeNode *root) {
        int sum = 0;
		if (root == NULL) {
			return sum;
		}

		DFS(root, sum);
		return sum;
    }

private:
	void DFS(TreeNode *root, int &sum) {
		stack<TreeNode *> dfs;
		dfs.push(root);
		TreeNode *prevNode = NULL;
		TreeNode *curNode = NULL;
		int curAmount = 0;

		while (!dfs.empty()) {
			curNode = dfs.top();
			if (prevNode == NULL || prevNode->left == curNode || prevNode->right == curNode) {
				curAmount = curAmount * 10 + curNode->val;
				if (curNode->left) {
					dfs.push(curNode->left);
				} else if (curNode->right) {
					dfs.push(curNode->right);
				} else {
					dfs.pop();
					sum += curAmount;
					curAmount = (curAmount - curNode->val) / 10;
				}
			} else if (curNode->left == prevNode) {
				if (curNode->right) {
					dfs.push(curNode->right);
				}
			} else {
				dfs.pop();
				curAmount = (curAmount - curNode->val) / 10;
			}
			prevNode = curNode;
		}
	}
};

#endif