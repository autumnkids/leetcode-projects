#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

class Solution {
public:
    int findPeakElement(const vector<int> &num) {
		int idx = -1;
		if (num.empty()) {
			return idx;
		} else if (num.size() == 1) {
			return num[0];
		}

		FindPeak(0, num.size() - 1, num, idx);
		return idx;
    }

private:
	void FindPeak(int start, int end, const vector<int> &num, int &idx) {
		if (start > end || idx != -1) {
			return ;
		}

		int mid = (start + end) / 2;
		if (mid - 1 < 0 && num[mid] > num[mid + 1]) {
			idx = mid;
			return ;
		}
		if (mid + 1 >= num.size() && num[mid] > num[mid - 1]) {
			idx = mid;
			return ;
		}
		if (num[mid] > num[mid - 1] && num[mid] > num[mid + 1]) {
			idx = mid;
			return ;
		}

		FindPeak(start, mid - 1, num, idx);
		FindPeak(mid + 1, end, num, idx);
	}
};

#endif