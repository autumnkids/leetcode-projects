#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <string>
using namespace std;

class Solution {
public:
    vector<string> restoreIpAddresses(string s) {
        vector<string> result;
        if(s.empty()) return result;
        string address;
        restoreIpAddressesUtil(result,s,address, 0, 1);
        return result;
    }

	void restoreIpAddressesUtil(vector<string> &result, string s, string address, int start, int len){
        if(start == s.size()){
            result.push_back(address.substr(0,address.size()-1));
        }else{
            for(int i = start; i < start+3 && i < s.size(); i++){
                int num = stoi(s.substr(start,i-start+1));
                if(num < 255 && (4-len)*3 >= (s.size() -i-1) && (s.size() -i-1) >= (4-len)){
                    address += s.substr(start,i-start+1);
                    address += '.';
                    restoreIpAddressesUtil(result,s,address,i+1,len+1);
                    address = address.substr(0,address.size()-(i-start+2));
                }
            }
        }
    }
	/*
    vector<string> restoreIpAddresses(string s) {
        vector<string> result;
		if (s.empty() || s.length() > 12 || s.length() < 4) {
			return result;
		}

		vector<string> curStr;
		AssignIP(result, curStr, s, 0, 1);
		return result;
    }
	*/

private:
	void AssignIP(vector<string> &result, vector<string> &curStr, string &s, int start, int pos) {
		if (curStr.size() == 4) {
			string output;
			for (int i = 0; i < 4; i++) {
				output = output + curStr[i];
				if (i != 3) {
					output = output + ".";
				}
			}
			result.push_back(output);
			return ;
		}
		if (start >= s.length()) {
			return ;
		}

		for (int i = 0; i < 3; i++) {
			if (start + i + 1 > s.length()) {
				continue;
			}
			string curNumS(s.begin() + start, s.begin() + start + i + 1);
			if (curNumS[0] == '0' && i > 0) {
				continue;
			}
			int curNum = atoi(curNumS.c_str());
			if (curNum >= 0 && curNum <= 255 && s.length() - start - i - 1 <= (4 - pos) * 3 && s.length() - start - i - 1 >= 4 - pos) {
				curStr.push_back(curNumS);
				AssignIP(result, curStr, s, start + i + 1, pos + 1);
				curStr.pop_back();
			}
		}
	}
};

#endif