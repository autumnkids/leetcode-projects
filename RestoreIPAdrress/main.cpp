#include <iostream>
#include "Solution.h"
using namespace std;

int main() {
	Solution sol;
	vector<string> result = sol.restoreIpAddresses("0000");
	for (int i = 0; i < result.size(); i++) {
		cout << result[i] << endl;
	}

	system("pause");
	return 0;
}