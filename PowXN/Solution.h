#ifndef _SOLUTION_H
#define _SOLUTION_H

class Solution {
public:
    double pow(double x, int n) {
		if (n < 0) {
			return 1 / Power(x, -n);
		}
		return Power(x, n);
    }

private:
	double Power(double x, int n) {
		if (n == 0) {
			return 1;
		}

		double v = Power(x, n / 2);
		if (n % 2 == 0) {
			return v * v;
		}
		return v * v * x;
	}
};

#endif