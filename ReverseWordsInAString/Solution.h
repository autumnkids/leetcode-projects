#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
#include <stack>
using namespace std;

class Solution {
public:
    void reverseWords(string &s) {
		int slen = s.length();
		if (slen == 0) {
			return ;
		}

		int start = 0;
		int end = slen - 1;
		while (start <= end) {
			// Skip all the spaces
			while (start < slen && s[start] == ' ') {
				start++;
			}
			while (end >= 0 && s[end] == ' ') {
				end--;
			}

			if (start < slen && end >= 0) {
				// Move end to the first character of the word
				int wlen = 0;
				while (end >= 0 && s[end] != ' ') {
					wlen++;
					end--;
				}
				end++;

				// Insert word to the front
				string word(s, end, wlen);
				s.erase(end, wlen);
				s.insert(start, " ");
				s.insert(start, word);
				start += wlen;
				end += wlen;
			}
		}

		// Clear spaces
		slen = s.length();
		start = 0; end = slen - 1;
		while (start < slen && s[start] == ' ') {
			start++;
		}
		while (end >= 0 && s[end] == ' ') {
			end--;
		}

		if (start <= end) {
			s.erase(s.begin() + end + 1, s.end());
			s.erase(s.begin(), s.begin() + start);
		} else {
			s.erase(s.begin(), s.end());
		}
    }
};

#endif