#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <algorithm>
using namespace std;

class Solution {
public:
	bool canJump(int A[], int n) {
		if (n == 0) {
			return true;
		}

		int maxGo = 0;
		for (int i = 0; i <= maxGo && i < n; i++) {
			if (A[i] + i > maxGo) {
				maxGo = A[i] + i;
			}
			if (maxGo >= n - 1) {
				return true;
			}
		}

		return false;
	}


	// Worst running time is O(n^2)
	/*
    bool canJump(int A[], int n) {
		if (n == 0) {
			return true;
		}

		bool *reach = new bool[n];
		memset(reach, 0, sizeof(reach));
		reach[0] = true;
		for (int i = 1; i < n; i++) {
			if (A[i - 1] != 0 && reach[i - 1]) {
				reach[i] = true;
			} else {
				int j = i - 1;
				while (j >= 0) {
					if (A[j] != 0 && reach[j] && i - j <= A[j]) {
						reach[i] = true;
						break;
					}
					j--;
				}
				if (j < 0) {
					reach[i] = false;
				}
			}
		}
		delete[] reach;

		return reach[n - 1];
    }
	*/
};

#endif