#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

class Solution {
public:
    int minimumTotal(vector<vector<int> > &triangle) {
		if (triangle.empty()) {
			return 0;
		}

		int row = triangle.size();
		int *minv = new int[row];
		for (int r = row - 1; r >= 0; r--) {
			int col = triangle[r].size();
			for (int c = 0; c < col; c++) {
				if (r == row - 1) {
					minv[c] = triangle[r][c];
					continue;
				}
				minv[c] = min(minv[c], minv[c + 1]) + triangle[r][c];
			}
		}

		return minv[0];
    }

private:
	inline int min(int a, int b) {
		return a <= b ? a : b;
	}
};

#endif