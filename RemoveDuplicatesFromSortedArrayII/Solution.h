#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

class Solution {
public:
    int removeDuplicates(int A[], int n) {
		if (n == 0 || n == 1) {
			return n;
		}

		int cur = 1, next = 1, duplicate = 0;
		while (next < n) {
			if (A[next] != A[cur - 1]) {
				duplicate = 0;
				A[cur] = A[next];
				cur++;
				next++;
			} else {
				duplicate++;
				if (duplicate >= 2) {
					while (next < n && A[next] == A[cur - 1]) {
						next++;
					}
				} else {
					A[cur] = A[next];
					cur++;
					next++;
				}
			}
		}

		return cur;
    }
};

#endif