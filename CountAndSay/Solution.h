#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
#include <iostream>
using namespace std;

class Solution {
public:
    string countAndSay(int n) {
        string output = "";
		if (n == 0) {
			return output;
		}

		string input;
		for (int i = 1; i <= n; i++) {
			if (i == 1) {
				output = "1";
			} else {
				output.clear();
				int slen = input.length();
				int count = 1;
				for (int j = 1; j < slen; j++) {
					if (input[j] == input[j - 1]) {
						count++;
					} else {
						output.append(1, char(count + '0'));
						output.append(1, input[j - 1]);
						count = 1;
					}
				}
				output.append(1, char(count + '0'));
				output.append(1, input[slen - 1]);
			}
			input = output;
		}

		return output;
    }
};

#endif