#include "Solution.h"

int main() {
	int n;
	Solution sol;
	while (cin >> n) {
		string ret;
		ret = sol.countAndSay(n);
		cout << ret << endl;
	}

	return 0;
}