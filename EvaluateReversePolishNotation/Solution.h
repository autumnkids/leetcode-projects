#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
#include <vector>
#include <stack>
using namespace std;

class Solution {
public:
    int evalRPN(vector<string> &tokens) {
		if (tokens.empty()) {
			return 0;
		}

		stack<int> numbers;
		int tSize = tokens.size();
		for (int i = 0; i < tSize; i++) {
			if (tokens[i] == "+" || tokens[i] == "-" || tokens[i] == "*" || tokens[i] == "/") {
				if (numbers.size() < 2) {
					return 0;
				}
				int num2 = numbers.top(); numbers.pop();
				int num1 = numbers.top(); numbers.pop();
				int sum = RunOperator(num1, num2, tokens[i][0]);
				numbers.push(sum);
			} else {
				int num = atoi(tokens[i].c_str());
				numbers.push(num);
			}
		}

		if (numbers.empty()) {
			return 0;
		}
		return numbers.top();
    }

private:
	int RunOperator(int a, int b, char op) {
		int ret = 0;
		switch(op) {
		case '+':
			ret = a + b;
			break;

		case '-':
			ret = a - b;
			break;

		case '*':
			ret = a * b;
			break;

		case '/':
			ret = a / b;
			break;
		}

		return ret;
	}
};

#endif