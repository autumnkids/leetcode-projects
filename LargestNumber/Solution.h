#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <algorithm>
#include <string>
using namespace std;

class Solution {
public:
	// Note:
	//		- Use string to combine two string, and compare
	//		- Remember to return a string with no leading zeros, after compare
	//		- Remember to check leading zeros at last
    string largestNumber(vector<int> &num) {
        string ret = "";
        if (num.empty()) {
            return ret;
        }
        
        int nSize = num.size();
        vector<string> numstrs;
        numstrs.push_back(to_string(num[0]));
        for (int i = 1; i < nSize; i++) {
            string numstr = to_string(num[i]);
            int j = 0, sSize = numstrs.size();
            for (; j < sSize; j++) {
				string atFront = numstr + numstrs[j];
				string atBack = numstrs[j] + numstr;
				string maxStr = max(atFront, atBack);
				if (maxStr == atFront) {
					numstrs.insert(numstrs.begin() + j, numstr);
					break;
				}
            }
            if (j == sSize) {
                numstrs.push_back(numstr);
            }
        }
        
        // Check zeros
        nSize = numstrs.size();
        int iter = 0;
        while (iter < nSize) {
            if (numstrs[iter] != "0") {
                break;
            }
            iter++;
        }
        if (iter == nSize) {
            numstrs.erase(numstrs.begin() + 1, numstrs.end());
        } else if (iter > 0) {
            numstrs.erase(numstrs.begin(), numstrs.begin() + iter);
        }
        
        nSize = numstrs.size();
        for (int i = 0; i < nSize; i++) {
            ret = ret + numstrs[i];
        }
        return ret;
    }

private:
	string max(string str1, string str2) {
		long long num1, num2;
		sscanf(str1.c_str(), "%lld", &num1);
		sscanf(str2.c_str(), "%lld", &num2);
		if (num1 >= num2) {
			return to_string(num1);
		}

		return to_string(num2);
	}
};

#endif