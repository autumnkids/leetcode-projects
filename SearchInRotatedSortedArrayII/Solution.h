#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

class Solution {
public:
    bool search(int A[], int n, int target) {
		if (n == 0) {
			return false;
		}

		bool ret = false;
		int start = 0;
		int end = n - 1;
		while (start <= end) {
			int mid = (start + end) / 2;
			if (A[mid] == target) {
				ret = true;
				break;
			}

			if (target < A[mid]) {
				if (target >= A[start] || A[start] > A[mid]) {
					end = mid - 1;
				} else if (A[start] == A[mid]) {
					start++;
				} else {
					start = mid + 1;
				}
			} else {
				if (target <= A[end] || A[mid] > A[end]) {
					start = mid + 1;
				} else if (A[mid] == A[end]) {
					end--;
				} else {
					end = mid - 1;
				}
			}
		}

		return ret;
    }
};

#endif