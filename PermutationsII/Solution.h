#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <algorithm>
#include <set>
using namespace std;

class Solution {
public:
    vector<vector<int> > permuteUnique(vector<int> &num) {
        vector<vector<int> > result;
		if (num.empty()) {
			return result;
		}
		set<vector<int> > records;
		sort(num.begin(), num.end());	// Remember to sort first
		result.push_back(num);			// Remember to push itself
		records.insert(num);

		while (FindNextPerm(num)) {
			if (records.find(num) == records.end()) {
				result.push_back(num);
			}
		}
		return result;
    }

private:
	bool FindNextPerm(vector<int> &num) {
		if (num.empty()) {
			return false;
		}

		int nSize = num.size();
		int mark = nSize - 2;
		while (mark >= 0) {
			if (num[mark] < num[mark + 1]) {
				break;
			}
			mark--;
		}
		if (mark < 0) {
			return false; // Finished
		}

		sort(num.begin() + mark + 1, num.end());
		for (int i = mark + 1; i < nSize; i++) {
			if (num[mark] < num[i]) {
				swap(num[mark], num[i]);
				break;
			}
		}
		return true;
	}
};

#endif