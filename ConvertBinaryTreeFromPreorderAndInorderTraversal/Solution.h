#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder) {
		if (preorder.empty() || inorder.empty()) {
			return NULL;
		}

		TreeNode *root = new TreeNode(0);
		BuildTree(root, preorder, 0, preorder.size() - 1, inorder, 0, inorder.size() - 1);
		return root;
    }

private:
	void BuildTree(TreeNode *root, vector<int> &preorder, int pstart, int pend, vector<int> &inorder, int istart, int iend) {
		root->val = preorder[pstart];

		// Find left tree
		int idx = istart;
		while (idx < inorder.size() && inorder[idx] != preorder[pstart]) {
			idx++;
		}
		if (idx - istart >= 1) {
			root->left = new TreeNode(0);
			BuildTree(root->left, preorder, pstart + 1, pstart + idx - istart, inorder, istart, idx - 1);
		}
		if (iend - idx >= 1) {
			root->right = new TreeNode(0);
			BuildTree(root->right, preorder, pstart + idx - istart + 1, pend, inorder, idx + 1, iend);
		}
	}
};

#endif