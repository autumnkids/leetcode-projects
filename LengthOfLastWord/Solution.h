#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <cstring>
using namespace std;

class Solution {
public:
    int lengthOfLastWord(const char *s) {
        int slen = strlen(s);
		if (slen == 0) {
			return 0;
		}

		int wordLen = 0;
		int lastWordLen = 0;
		for (int i = 0; i < slen; i++) {
			if (s[i] == ' ') {
				lastWordLen = wordLen;
				wordLen = 0;
				while (i < slen && s[i] == ' ') {
					i++;
				}
				i--;
			} else {
				wordLen++;
			}
		}

		if (wordLen != 0 && lastWordLen != wordLen) {
			lastWordLen = wordLen;
		}

		return lastWordLen;
    }
};

#endif