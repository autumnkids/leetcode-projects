#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
#include <vector>
using namespace std;

class Solution {
public:
    int longestValidParentheses(string s) {
		const char *str = s.c_str();
		const char *p = str;
		vector<const char *> stk;
		int nMax = 0;
		while ((* p) != '\0') {
			if ((* p) == '(') {
				stk.push_back(p);
			} else {
				if (!stk.empty() && (* stk.back()) == '(') {
					stk.pop_back();
					nMax = max(nMax, p - (stk.empty() ? str - 1 : stk.back()));
				} else {
					stk.push_back(p);
				}
			}
			p++;
		}

		return nMax;
    }
};

#endif