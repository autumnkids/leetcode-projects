#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

class Solution {
public:
    vector<vector<int> > combine(int n, int k) {
        vector<vector<int> > result;
		vector<int> output;
		if (n == 0) {
			output.push_back(0);
			result.push_back(output);
			return result;
		}

		BackTracing(result, output, n, k, 1);
		return result;
    }

private:
	void BackTracing(vector<vector<int> > &result, vector<int> &output, int n, int k, int idx) {
		if (output.size() == k) {
			result.push_back(output);
			return ;
		}

		for (int i = idx; i <= n; i++) {
			output.push_back(i);
			BackTracing(result, output, n, k, i + 1);
			output.pop_back();
		}
	}
};

#endif