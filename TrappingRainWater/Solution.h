#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <algorithm>
using namespace std;

class Solution {
public:
    int trap(int A[], int n) {
		if (n == 0) {
			return 0;
		}

		int sum = 0;
		int *maxL = new int[n];
		int *maxR = new int[n];
		int max = A[0];
		maxL[0] = 0;
		for (int i = 1; i < n; i++) {
			maxL[i] = max;
			if (max < A[i]) {
				max = A[i];
			}
		}
		max = A[n - 1];
		for (int i = n - 2; i >= 0; i--) {
			maxR[i] = max;
			if (max < A[i]) {
				max = A[i];
			}
			int trap = min(maxL[i], maxR[i]) - A[i];
			if (trap > 0) {
				sum += trap;
			}
		}
		delete maxL, maxR;

		return sum;
    }
};

#endif