#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
using namespace std;

class Solution {
public:
    string minWindow(string S, string T) {
        if (S.size() < T.size() || S.empty() || T.empty()) {
            return "";
        }

		int expectedCount[256];
		int appearCount[256];
		memset(expectedCount, 0, sizeof(expectedCount));
		memset(appearCount, 0, sizeof(appearCount));
		int tlen = T.length();
		int slen = S.length();
		for (int i = 0; i < tlen; i++) {
			expectedCount[T[i]]++;
		}

		int wid_end = 0, wid_start = 0, appeared = 0, minLen = INT_MAX, minStart = 0;
		while (wid_end < slen) {
			if (expectedCount[S[wid_end]] > 0) {
				appearCount[S[wid_end]]++;
				if (appearCount[S[wid_end]] <= expectedCount[S[wid_end]]) {
					appeared++;
				}
			}
			if (appeared == tlen) {
				while (appearCount[S[wid_start]] > expectedCount[S[wid_start]] || expectedCount[S[wid_start]] == 0) {
					appearCount[S[wid_start]]--;
					wid_start++;
				}
				if (minLen > wid_end - wid_start + 1) {
					minLen = wid_end - wid_start + 1;
					minStart = wid_start;
				}
			}
			wid_end++;
		}

		if (minLen == INT_MAX) {
			return "";
		}
		return S.substr(minStart, minLen);
    }
};

#endif