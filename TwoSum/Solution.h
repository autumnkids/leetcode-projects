#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <string>
#include <algorithm>
using namespace std;

class Solution {
public:
	struct Num {
		int val;
		int pos;
	};

	struct NumOp {
		bool operator()(const Num &a, const Num &b) 
		{ return a.val < b.val; }
	}numOp;

    vector<int> twoSum(vector<int> &numbers, int target) {
		int nSize = numbers.size();
		vector<int> result;
		if (nSize < 2) {
			return result;
		}

		vector<Num> nums;
		for (int i = 0; i < nSize; i++) {
			Num n; n.val = numbers[i]; n.pos = i + 1;
			nums.push_back(n);
		}
		sort(nums.begin(), nums.end(), numOp);
		
		int start = 0;
		int end = nSize - 1;
		while (start < end) {
			int rest = nums[start].val + nums[end].val - target;
			if (rest == 0) {
				int startIdx = nums[start].pos <= nums[end].pos ? nums[start].pos : nums[end].pos;
				int endIdx = nums[start].pos < nums[end].pos ? nums[end].pos : nums[start].pos;
				result.push_back(startIdx);
				result.push_back(endIdx);
				break;
			} else if (rest > 0) {
				end--;
			} else {
				start++;
			}
		}

		return result;
    }
};

#endif