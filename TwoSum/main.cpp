#include <iostream>
#include "Solution.h"

int main() {
	vector<int> num;
	num.push_back(0); num.push_back(4); num.push_back(3); num.push_back(0);
	Solution sol;
	vector<int> result = sol.twoSum(num, 0);
	for (int i = 0; i < 2; i++) {
		cout << result[i] << endl;
	}

	system("pause");
	return 0;
}