#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
#include <limits>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *sortList(ListNode *head) {
		if (head == NULL) {
			return NULL;
		}

		int length = 0;
		ListNode *curNode = head;
		while (curNode != NULL) {
			curNode = curNode->next;
			length++;
		}

		return Sort(&head, length);
    }

private:
	ListNode *Sort(ListNode **head, int length) {
		if (length == 1) {
			ListNode *node = (* head);
			(* head) = (* head)->next;
			node->next = NULL;
			return node;
		}

		ListNode *leftPart = Sort(head, length / 2);
		ListNode *rightPart = Sort(head, length - length / 2);
		ListNode *merge = Merge(leftPart, rightPart);

		return merge;
	}

	ListNode *Merge(ListNode *first, ListNode *second) {
		ListNode *head = new ListNode(-1);
		ListNode *curNode = head;

		while (first != NULL || second != NULL) {
			int fv = first == NULL ? INT_MAX : first->val;
			int sv = second == NULL ? INT_MAX : second->val;

			if (fv <= sv) {
				curNode->next = first;
				first = first->next;
			} else {
				curNode->next = second;
				second = second->next;
			}

			curNode = curNode->next;
		}

		curNode = head->next;
		delete head;
		return curNode;
	}
};

#endif