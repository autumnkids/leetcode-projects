#ifndef _SOLUTION_H
#define _SOLUTION_H

using namespace std;

class Solution {
public:
    int sqrt(int x) {
		if (x == 1) {
			return 1;
		}

        int start = 1;
		int end = x >> 1;
		long long mid = 0, max_mid = 0;
		while (start <= end) {
			mid = (start + end) >> 1;
			long long res = mid * mid;
			if (res == x) {
				return mid;
			}
			if (res < x) {
				if (max_mid < mid) {
					max_mid = mid;
				}
				start = mid + 1;
			} else {
				end = mid - 1;
			}
		}

		return max_mid;
    }
};

#endif