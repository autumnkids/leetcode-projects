#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
using namespace std;

class Solution {
public:
    int titleToNumber(string s) {
		int slen = s.size();
		int sum = 0;
		if (slen == 0) {
			return sum;
		}

		for (int i = 0; i < slen; i++) {
			sum *= 26;
			sum += int(s[i] - 'A') + 1;
		}

		return sum;
    }
};

#endif