#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *rotateRight(ListNode *head, int k) {
		if (head == NULL) {
			return NULL;
		}

		int len = 0;
		ListNode *dummy = new ListNode(-1);
		dummy->next = head;
		ListNode *cur = head;
		while (cur != NULL) {
			len++;
			cur = cur->next;
		}
		k = k % len;
		if (k == 0) {
			return head;
		}

		int count = 1;
		cur = head;
		while (count < k) {
			count++;
			cur = cur->next;
		}
		ListNode *left = dummy;
		ListNode *right = cur;
		while (right->next != NULL) {
			left = left->next;
			right = right->next;
		}
		dummy->next = left->next;
		right->next = head;
		left->next = NULL;
		
		head = dummy->next;
		delete dummy;
		return head;
    }
};

#endif