#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

class Solution {
public:
    void sortColors(int A[], int n) {
		if (n == 0) {
			return ;
		}

		int redIdx = 0;
		int blueIdx = n - 1;
		int i = 0;
		while (i < blueIdx + 1) {
			if (A[i] == 0) {
				swap(A[i], A[redIdx]);
				redIdx++;
			} else if (A[i] == 2) {
				swap(A[i], A[blueIdx]);
				blueIdx--;
				continue;
			}
			i++;
		}
    }
};

#endif