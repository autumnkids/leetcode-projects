#ifndef _SOLUTION_H_
#define _SOLUTION_H_

#include <algorithm>
#include <vector>
#include <limits>
using namespace std;

class Solution {
public:
	/*
    int majorityElement(vector<int> &num) {
		int maxSize = -INT_MAX;
		int lastNum = num[0];
		int size = 1; // Appear times of each number
		int result = lastNum;

        // Sort the array first
        sort(num.begin(), num.end());
        
        // Count appear times
		for (vector<int>::iterator iter = num.begin() + 1; iter != num.end(); iter++) {
			if (lastNum == (* iter)) {
				size++;
			} else {
				size = 1;
			}
			lastNum = (* iter);

			if (size > maxSize) {
				maxSize = size;
				result = (* iter);
			}
		}

		return result;
    }
	*/

	int majorityElement(vector<int> &num) {
		int result = num[0];
		int count = 1;

        // Count appear times
		for (vector<int>::iterator iter = num.begin() + 1; iter != num.end(); iter++) {
			if (result == (* iter)) {
				count++;
			} else {
				count--;
			}

			if (count == 0) {
				result = (* iter);
				count = 1;
			}
		}

		return result;
    }
};

#endif