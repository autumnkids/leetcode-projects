#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <string>
using namespace std;

class Solution {
public:
    vector<vector<string> > solveNQueens(int n) {
        vector<vector<string> > result;
		if (n == 0) {
			return result;
		}

		vector<string> output(n, string(n, '.'));
		TryOn(0, n, result, output);
		return result;
    }

private:
	void TryOn(int row, int n, vector<vector<string> > &result, vector<string> &output) {
		if (row == n) {
			result.push_back(output);
			return ;
		}

		for (int j = 0; j < n; j++) {
			if (IsValid(row, j, n, output)) {
				output[row][j] = 'Q';
				TryOn(row + 1, n, result, output);
				output[row][j] = '.';
			}
		}
	}

	bool IsValid(int row, int col, int n, vector<string> &output) {
		// Check for columns
		for (int i = 0; i < row; i++) {
			if (output[i][col] == 'Q') { return false; }
		}

		// Check for diagonals
		for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; i--, j--) {
			if (output[i][j] == 'Q') { return false; }
		}
		for (int i = row - 1, j = col + 1; i >= 0 && j < n; i--, j++) {
			if (output[i][j] == 'Q') { return false; }
		}

		return true;
	}
};

#endif