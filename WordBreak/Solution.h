#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
#include <unordered_set>
using namespace std;

class Solution {
public:
	// Notes: DP
	//		- possible[i], s[0, i] is true, if can find a word in dictionary
	//		- possible[i], s[0, i] is true, if possible[k] is true, and s[k + 1, i] can be found in dictionary
	//		- is false, if nothing found in dictionary
    bool wordBreak(string s, unordered_set<string> &dict) {
		if (s.empty()) {
			return false;
		}

		s = "#" + s;
		int slen = s.length();
		vector<bool> possible(slen, 0);
		possible[0] = true;
		for (int i = 1; i < slen; i++) {
			for (int k = 0; k < i; k++) {
				possible[i] = possible[k] && (dict.find(s.substr(k + 1, i - k)) != dict.end());
				if (possible[i]) {
					break;
				}
			}
		}

		return possible[slen - 1];
    }
};

#endif