#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
#include <unordered_map>
using namespace std;

class Solution {
public:
    string fractionToDecimal(int numerator, int denominator) {
		// If the fraction start to repeat, insert the parentheses
		// Use long long, to avoid overflow
		long long num = (long long)numerator;
		long long den = (long long)denominator;
		
		bool isPosN = numerator >= 0 ? true : false;
		bool isPosD = denominator >= 0 ? true : false;
		char sign = ((isPosN && isPosD) || (!isPosN && !isPosD)) ? '+' : '-';
		if (!isPosN) {
			num *= -1;
		}
		if (!isPosD) {
			den *= -1;
		}
		long long int_frac = num / den;
		int rest = num % den;
		string result = to_string(int_frac);

		// Fraction part
		string frac = "";
		unordered_map<int, int> dict;
		for (int pos = 0; rest != 0; pos++) {
			if (dict.find(rest) != dict.end()) {
				frac.insert(dict[rest], "(");
				frac = frac + ")";
				break;
			}

			dict[rest] = pos;
			frac = frac + to_string(rest * 10 / den);
			rest = (rest * 10) % den;
		}

		if (frac != "" && frac != "0") {
			result = result + "." + frac;
		}
		if (sign == '-' && result != "0") {
			result = "-" + result;
		}

		return result;
    }
};

#endif