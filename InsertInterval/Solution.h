#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <algorithm>
using namespace std;

struct Interval {
    int start;
    int end;
    Interval() : start(0), end(0) {}
    Interval(int s, int e) : start(s), end(e) {}
};

class Solution {
public:
	vector<Interval> insert(vector<Interval> &intervals, Interval newInterval) {
		vector<Interval> result;
		if (intervals.empty()) {
			result.push_back(newInterval);
			return result;
		}

		result.push_back(newInterval);
		int nSize = intervals.size();
		for (int i = 0; i < nSize; i++) {
			Interval cur = result.back();
			result.pop_back();
			if (intervals[i].end < cur.start) {
				result.push_back(intervals[i]);
				result.push_back(cur);
			} else if (intervals[i].start > cur.end) {
				result.push_back(cur);
				result.push_back(intervals[i]);
			} else {
				cur.start = min(intervals[i].start, cur.start);
				cur.end = max(intervals[i].end, cur.end);
				result.push_back(cur);
			}
		}
		return result;
	}

	/*
    vector<Interval> insert(vector<Interval> &intervals, Interval newInterval) {
        vector<Interval> result;
		if (intervals.empty()) {
			result.push_back(newInterval);
			return result;
		}

		int nSize = intervals.size();
		int newStartIdx = -1, newEndIdx = -1;
		int newStart, newEnd;
		bool alreadyInsert = false;
		int i = 0;
		while (i < nSize) {
			if (newStartIdx == -1 && intervals[i].end >= newInterval.start) {
				newStart = min(newInterval.start, intervals[i].start);
				newStartIdx = i;
			}
			if (newEndIdx == -1 && intervals[i].end >= newInterval.end) {
				newEndIdx = i;
				if (intervals[i].start <= newInterval.end) {
					newEnd = max(newInterval.end, intervals[i].end);
				} else if (intervals[i].start > newInterval.end) {
					newEnd = newInterval.end;
					i--;
				}
			}
			if (!alreadyInsert && newStartIdx != -1 && newEndIdx != -1) {
				Interval insertInt(newStart, newEnd);
				result.push_back(insertInt);
				alreadyInsert = true;
				i++;
				break;
			} else if (newStartIdx == -1 && newEndIdx == -1) {
				result.push_back(intervals[i]);
			}
			i++;
		}
		while (i < nSize) {
			result.push_back(intervals[i]);
			i++;
		}

		if (newStartIdx == -1 && newEndIdx == -1) {
			result.push_back(newInterval);
		} else if (newEndIdx == -1) {
			Interval insertInt(newStart, newInterval.end);
			result.push_back(insertInt);
		}

		return result;
    }
	*/
};

#endif