#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

class Solution {
public:
    void merge(int A[], int m, int B[], int n) {
        // Move A to the end first
		for (int i = m - 1; i >= 0; i--) {
			A[i + n] = A[i];
		}

		// Compare A and B
		int i = 0, j = 0, k = 0;
		for (i = n, j = 0, k = 0; i < m + n && j < n; k++) {
			if (A[i] < B[j]) {
				A[k] = A[i];
				i++;
			} else if (A[i] > B[j]) {
				A[k] = B[j];
				j++;
			} else {
				A[k] = A[i]; i++;
				k++;
				A[k] = B[j]; j++;
			}
		}

		if (i >= m + n && j < n) {
			while (j < n) {
				A[k] = B[j];
				k++;
				j++;
			}
		}
    }
};

#endif