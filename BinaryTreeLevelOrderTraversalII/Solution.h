#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <stack>
#include <queue>
#include <cmath>
#include <iostream>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    vector<vector<int> > levelOrderBottom(TreeNode *root) {
        vector<vector<int> > result;
		if (root == NULL) {
			return result;
		}

		stack<vector<int> > rev_result;
		PutTreeIntoVector(root, rev_result);

		// Output to the result
		while (!rev_result.empty()) {
			result.push_back(rev_result.top());
			rev_result.pop();
		}

		return result;
    }

private:
	void PutTreeIntoVector(TreeNode *root, stack<vector<int> > &result) {
		queue<TreeNode *> bfs;
		bfs.push(root);
		bfs.push(NULL);

		vector<int> level;
		while (!bfs.empty()) {
			TreeNode *frontNode = bfs.front();
			bfs.pop();

			if (frontNode == NULL) {
				if (!bfs.empty()) {
					bfs.push(NULL);
				}
				result.push(level);
				level.clear();
			} else {
				level.push_back(frontNode->val);
				if (frontNode->left) {
					bfs.push(frontNode->left);
				}
				if (frontNode->right) {
					bfs.push(frontNode->right);
				}
			}
		}
	}
};

#endif