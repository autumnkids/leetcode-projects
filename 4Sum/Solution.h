#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <set>
#include <algorithm>
using namespace std;

class Solution {
public:
    vector<vector<int> > fourSum(vector<int> &num, int target) {
        vector<vector<int> > result;
		int nSize = num.size();
		if (nSize == 0) {
			return result;
		}

		sort(num.begin(), num.end());
		set<vector<int> > found;
		for (int i = 0; i < nSize - 3; i++) {
			for (int j = i + 1; j < nSize - 2; j++) {
				int start = j + 1;
				int end = nSize - 1;
				int rest = num[i] + num[j] - target;
				while (start < end) {
					if (rest + num[start] + num[end] == 0) {
						vector<int> output;
						output.push_back(num[i]);
						output.push_back(num[j]);
						output.push_back(num[start]);
						output.push_back(num[end]);
						if (found.find(output) == found.end()) {
							result.push_back(output);
							found.insert(output);
						}
						start++;
						end--;
					} else if (rest + num[start] + num[end] < 0) {
						start++;
					} else {
						end--;
					}
				}
			}
		}

		return result;
    }
};

#endif