#ifndef _SOLUTION_H_
#define _SOLUTION_H_

class Solution {
public:
    int maxProduct(int A[], int n) {
        int result = A[0];
		int pos = max(0, A[0]);
		int neg = min(0, A[0]);

		for (int i = 1; i < n; i++) {
			if (A[i] == 0) {
				pos = 0;
				neg = 0;
			} else if (A[i] > 0) {
				pos = max(1, pos) * A[i];
				neg *= A[i];
			} else {
				int prevPos = pos;
				pos = neg * A[i];
				neg = min(A[i], A[i] * prevPos);
			}

			result = max(result, pos);
		}

		return result;
    }

private:
	inline int max(int a, int b) {
		return a > b ? a : b;
	}

	inline int min(int a, int b) {
		return a < b ? a : b;
	}
};

#endif