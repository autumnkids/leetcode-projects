#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <set>
#include <algorithm>
using namespace std;

class Solution {
public:
    vector<vector<int> > combinationSum(vector<int> &candidates, int target) {
        vector<vector<int> > result;
		vector<int> output;
		set<vector<int> > records;
		if (candidates.empty()) {
			return result;
		}

		sort(candidates.begin(), candidates.end());
		BackTracing(result, output, candidates, records, target, 0, 0);
		return result;
    }

private:
	void BackTracing(vector<vector<int> > &result, vector<int> &output, vector<int> &candidates, set<vector<int> > &records, int target, int curIdx, int sum) {
		if (sum == target) {
			if (records.find(output) == records.end()) {
				result.push_back(output);
				records.insert(output);
			}
			return ;
		}
		if (sum > target || curIdx >= candidates.size()) {
			return ;
		}

		int nlen = candidates.size();
		for (int i = curIdx; i < nlen; i++) {
			sum += candidates[i];
			output.push_back(candidates[i]);
			BackTracing(result, output, candidates, records, target, i, sum);
			sum -= candidates[i];
			output.pop_back();
		}
	}
};

#endif