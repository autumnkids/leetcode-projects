#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
#include <map>
#include <vector>
#include <algorithm>
using namespace std;

class Solution {
public:
    vector<string> anagrams(vector<string> &strs) {
        vector<string> result;
		if (strs.empty()) {
			return result;
		}

		map<string, int> words;
		int sSize = strs.size();
		for (int i = 0; i < sSize; i++) {
			string str = strs[i];
			sort(str.begin(), str.end());
			if (words.find(str) == words.end()) {
				words[str] = i;
			} else {
				if (words[str] >= 0) {
					result.push_back(strs[words[str]]);
					words[str] = -1;
				}
				result.push_back(strs[i]);
			}
		}

		return result;
    }
};

#endif