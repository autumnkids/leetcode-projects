#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

class Solution {
public:
    bool searchMatrix(vector<vector<int> > &matrix, int target) {
		if (matrix.empty() || matrix[0].empty()) {
			return false;
		}

		int rSize = matrix.size();
		int cSize = matrix[0].size();
		// Search row
		int rstart = 0, rend = rSize - 1, targetRow = -1;
		while (rstart <= rend) {
			int mid = (rstart + rend) / 2;
			if (matrix[mid][0] == target) {
				return true;
			}
			if (target > matrix[mid][0]) {
				if ((mid < rSize - 1 && target < matrix[mid + 1][0]) || (mid == rSize - 1)) {
					targetRow = mid;
					break;
				} else {
					rstart = mid + 1;
				}
			} else {
				rend = mid - 1;
			}
		}
		if (targetRow == -1) {
			return false;
		}

		// Search in columns
		int cstart = 0, cend = cSize - 1;
		while (cstart <= cend) {
			int mid = (cstart + cend) / 2;
			if (matrix[targetRow][mid] == target) {
				return true;
			}
			if (target > matrix[targetRow][mid]) {
				cstart = mid + 1;
			} else {
				cend = mid - 1;
			}
		}

		if (cstart > cend) {
			return false;
		}
		return true;
    }
};

#endif