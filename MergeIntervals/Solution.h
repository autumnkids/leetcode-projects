#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <algorithm>
using namespace std;

struct Interval {
    int start;
    int end;
    Interval() : start(0), end(0) {}
    Interval(int s, int e) : start(s), end(e) {}
};

class Solution {
public:
	vector<Interval> merge(vector<Interval> &intervals) {
		if (intervals.empty() || intervals.size() == 1) {
			return intervals;
		}

		sort(intervals.begin(), intervals.end(), [](const Interval &a, const Interval &b) {
			return a.start < b.start;
		});
		vector<Interval> result;
		int nSize = intervals.size();
		result.push_back(intervals[0]);
		for (int i = 1; i < nSize; i++) {
			Interval last = result.back();
			result.pop_back();
			if (last.end < intervals[i].start) {
				result.push_back(last);
				result.push_back(intervals[i]);
			} else if (last.start > intervals[i].end) {
				result.push_back(intervals[i]);
				result.push_back(last);
			} else {
				last.start = min(intervals[i].start, last.start);
				last.end = max(intervals[i].end, last.end);
				result.push_back(last);
			}
		}
		return result;
	}

	/*
    vector<Interval> merge(vector<Interval> &intervals) {
        vector<Interval> result;
		if (intervals.empty()) {
			return result;
		}

		sort(intervals.begin(), intervals.end(), [](const Interval &a, const Interval &b) {
			return a.start < b.start;
		});
		int nSize = intervals.size();
		int start, end;
		for (int i = 1; i < nSize; i++) {
			start = intervals[i - 1].start;
			end = intervals[i - 1].end;
			while (i < nSize && intervals[i].start <= end) {
				end = max(end, intervals[i].end);
				i++;
			}
			Interval merge(start, end);
			result.push_back(merge);
			if (i >= nSize) {
				return result;
			}
		}
		Interval last(intervals[nSize - 1].start, intervals[nSize - 1].end);
		result.push_back(last);

		return result;
    }
	*/
};

#endif