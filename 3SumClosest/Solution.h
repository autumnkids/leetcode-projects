#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <cmath>
#include <limits>
#include <algorithm>
using namespace std;

class Solution {
public:
    int threeSumClosest(vector<int> &num, int target) {
		if (num.empty()) {
			return 0;
		}

		int retSum = 0;
		int nlen = num.size();
		int minDiff = INT_MAX;
		sort(num.begin(), num.end());
		for (int i = 0; i < nlen; i++) {
			int start = i + 1;
			int end = nlen - 1;
			while (start < end) {
				int diff = num[i] + num[start] + num[end] - target;
				if (diff == 0) {
					return target;
				}

				if (abs(diff) < minDiff) {
					minDiff = abs(diff);
					retSum = num[i] + num[start] + num[end];
				}

				if (diff < 0) {
					start++;
				} else {
					end--;
				}
			}
		}

		return retSum;
    }
};

#endif