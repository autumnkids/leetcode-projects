#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
#include <unordered_map>
#include <cmath>
#include <algorithm>
using namespace std;

class Solution {
public:
    string intToRoman(int num) {
		char marks[] = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
		int scale = 1000;
		string ret;
		for (int i = 6; i >= 0; i -= 2) {
			int digit = num / scale;
			if (digit != 0) {
				if (digit >= 1 && digit <= 3) {
					ret.append(digit, marks[i]);
				} else if (digit == 4) {
					ret.append(1, marks[i]);
					ret.append(1, marks[i + 1]);
				} else if (digit == 5) {
					ret.append(1, marks[i + 1]);
				} else if (digit <= 8) { // [6, 8]
					ret.append(1, marks[i + 1]);
					ret.append(digit - 5, marks[i]);
				} else if (digit == 9) {
					ret.append(1, marks[i]);
					ret.append(1, marks[i + 2]);
				}
			}
			num = num % scale;
			scale /= 10;
		}

		return ret;
    }
};

#endif