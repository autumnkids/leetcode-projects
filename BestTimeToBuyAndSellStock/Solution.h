#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

class Solution {
public:
    int maxProfit(vector<int> &prices) {
		int nSize = prices.size();
		int result = 0;
		if (nSize == 0) {
			return result;
		}

		int maxDiff = 0;
		int min = prices[0];
		for (int i = 1; i < nSize; i++) {
			if (min > prices[i]) {
				min = prices[i];
			}
			if (maxDiff < prices[i] - min) {
				maxDiff = prices[i] - min;
			}
		}

		return maxDiff;
    }
};

#endif