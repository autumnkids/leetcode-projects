#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

class Solution {
public:
    vector<int> spiralOrder(vector<vector<int> > &matrix) {
        vector<int> result;
		if (matrix.empty() || matrix[0].empty()) {
			return result;
		}

		int columns[] = {1, 0, -1, 0};
		int rows[]    =	{0, 1, 0, -1};
		int direction = 0;
		int candidateNum = 0;
		int row = matrix.size();
		int col = matrix[0].size();
		int visitedRow = 0;
		int visitedCol = 0;
		int r = 0, c = 0;
		int moveStep = 0;
		while (true) {
			if (columns[direction] == 0) {
				candidateNum = row - visitedRow;
			} else {
				candidateNum = col - visitedCol;
			}

			if (candidateNum <= 0) {
				break;
			}
			result.push_back(matrix[r][c]);
			moveStep++;
			if (candidateNum == moveStep) {
				visitedRow += columns[direction] == 0 ? 0 : 1;
				visitedCol += rows[direction] == 0 ? 0 : 1;
				direction = (direction + 1) % 4;
				moveStep = 0;
			}
			r += rows[direction];
			c += columns[direction];
		}
		return result;
    }
};

#endif