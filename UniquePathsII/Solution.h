#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int> > &obstacleGrid) {
        vector<vector<int> > copyGrid = obstacleGrid;
		if (obstacleGrid.empty()) {
			return 0;
		}

		int rSize = copyGrid.size();
		int cSize = copyGrid[0].size();
		for (int i = 0; i < rSize; i++) {
			for (int j = 0; j < cSize; j++) {
				if (obstacleGrid[i][j] == 1) {
					copyGrid[i][j] = 0;
				} else if (i == 0) {
					copyGrid[i][j] = j == 0 ? 1 : copyGrid[i][j - 1];
				} else if (j == 0) {
					copyGrid[i][j] = i == 0 ? 1 : copyGrid[i - 1][j];
				} else {
					copyGrid[i][j] = copyGrid[i - 1][j] + copyGrid[i][j - 1];
				}
			}
		}
		return copyGrid[rSize - 1][cSize - 1];
    }
};

#endif