#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *reverseBetween(ListNode *head, int m, int n) {
		if (head == NULL) {
			return NULL;
		} else if (head->next == NULL) {
			return head;
		}

		ListNode *prevNode = new ListNode(0); // Create a dummy node
		ListNode *curNode = head;
		ListNode *nextNode = head->next;
		prevNode->next = head;
		head = prevNode;

		// Move three pointers to specific position
		int i = 1;
		while (i < n) {
			if (i >= m) {
				ListNode *node = nextNode->next;
				nextNode->next = curNode;
				curNode = nextNode;
				nextNode = node;
			} else {
				prevNode = curNode;
				curNode = nextNode;
				nextNode = nextNode->next;
			}
			i++;
		}
		ListNode *node = prevNode->next;
		prevNode->next = curNode;
		node->next = nextNode;
		
		// Delete dummy node
		node = head;
		head = head->next;
		delete node;

		return head;
    }
};

#endif