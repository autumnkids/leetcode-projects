#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <string>
using namespace std;

class Solution {
public:
	// Note:
	//		- 3 conditions:
	//			- The current strings + spaces == L, then push into result
	//			- The current strings + spaces > L, then pop the last one, and calculate spaces, then push into result
	//			- i >= words.size() and rowSize still != 0, then it is the last line, add spaces to the end of line, and push into result
	//			- Else, push back the word to the buffer
	// Special cases:
	//		- If has one word, push spaces back to fill the line
	//		- If the spaces can't be filled in evenly, plus one from left to right
    vector<string> fullJustify(vector<string> &words, int L) {
		if (words.empty()) {
			return words;
		}
		vector<string> result;
		if (words.size() == 1 && words[0] == "") {
			string outStr; outStr.append(L, ' ');
			result.push_back(outStr);
			return result;
		}

		int wSize = words.size();
		int rowSize = 0;
		int i = 0;
		vector<string> output; // For each row
		while (i < wSize || rowSize != 0) {
			int size = output.size();
			if (rowSize + size - 1 == L) {
				// Put into result
				string outStr;
				int spaces = L - rowSize;
				CreateLine(outStr, output, size, spaces, 0);
				result.push_back(outStr);
				output.clear();
				rowSize = 0;
			} else if (rowSize + size - 1 > L) {
				// Deal with the spaces
				rowSize -= output.back().length();
				output.pop_back();
				i--;
				size = output.size();
				int spaces = L - rowSize;
				string outStr;
				CreateLine(outStr, output, size, spaces, 0);
				result.push_back(outStr);
				output.clear();
				rowSize = 0;
			} else if (i >= wSize) {
				// The last line
				int spaces = L - rowSize - size + 1;
				string outStr;
				CreateLine(outStr, output, size, size - 1, spaces);
				result.push_back(outStr);
				output.clear();
				rowSize = 0;
			} else {
				int wlen = words[i].length();
				rowSize += wlen;
				output.push_back(words[i]);
				i++;
			}
		}
		return result;
    }

private:
	void CreateLine(string &outStr, vector<string> &output, int size, int spaces, int backup_spaces) {
		if (size == 1) {
			outStr = outStr + output[0];
			outStr.append(spaces, ' ');
		} else {
			int avg_spaces = spaces / (size - 1);
			int rest_spaces = spaces % (size - 1);
			for (int j = 0; j < size; j++) {
				outStr = outStr + output[j];
				if (j < size - 1) {
					int offset = rest_spaces > 0 ? 1 : 0;
					outStr.append(avg_spaces + offset, ' ');
					rest_spaces = rest_spaces > 0 ? rest_spaces - 1 : 0;
				}
			}
		}
		outStr.append(backup_spaces, ' ');
	}
};

#endif