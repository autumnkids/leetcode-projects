#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
#include <algorithm>
using namespace std;

class Solution {
public:
	// Bucket sort
    int firstMissingPositive(int A[], int n) {
		for (int i = 0; i < n; i++) {
			while (A[i] != i + 1) { // Save A[i] to A[A[i]]
				if (A[i] <= 0 || A[i] > n || A[i] == A[A[i] - 1]) {
					break;
				}
				int tmp = A[i];
				A[i] = A[tmp - 1];
				A[tmp - 1] = tmp;
			}
		}
		for (int i = 0; i < n; i++) {
			if (A[i] != i + 1) {
				return i + 1;
			}
		}
		return n + 1;
    }
};

#endif