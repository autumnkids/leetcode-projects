#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <cstring>
#include <iostream>
using namespace std;

// Can use KMP

class Solution {
public:
    int strStr(char *haystack, char *needle) {
		int lenStr = strlen(haystack);
		int lenSub = strlen(needle);
		if (lenSub > lenStr) {
			return -1;
		} else if ((lenSub == 0 && lenStr == 0) || lenSub == 0) {
			return 0;
		}

		for (int i = 0; i < lenStr - lenSub + 1; i++) {
			if (haystack[i] == needle[0]) {
				// Check the rest
				int j = 1;
				for (j = 1; j < lenSub; j++) {
					if (haystack[i + j] != needle[j]) {
						break;
					}
				}
				if (j == lenSub) {
					return i;
				}
			}
		}

		return -1;
    }
};

#endif