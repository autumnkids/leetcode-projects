#include <iostream>
#include "Solution.h"

int main() {
	int n;
	Solution sol;
	while (cin >> n) {
		string s = sol.convertToTitle(n);
		cout << s << endl;
	}
	return 0;
}