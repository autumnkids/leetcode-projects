#ifndef _SOLUTION_H_
#define _SOLUTION_H_

#include <string>
using namespace std;

class Solution {
public:
    string convertToTitle(int n) {
		string result;
		int quotient = n;

		while (quotient != 0) {
			int mod = quotient % 26;
			mod = mod == 0 ? 26 : mod;
			char ch[2];
			ch[0] = char('A' + mod - 1);
			ch[1] = '\0';
			result.insert(0, ch);

			quotient = quotient / 26;
			quotient = mod == 26 ? quotient - 1 : quotient;
		}

		return result;
    }
};

#endif