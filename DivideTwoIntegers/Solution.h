#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <limits>
using namespace std;

class Solution {
public:
    int divide(int dividend, int divisor) {
		if (divisor == 0) {
			return INT_MAX;
		}

		unsigned int dvd = dividend < 0 ? -dividend : dividend;
		unsigned int dvs = divisor < 0 ? -divisor : divisor;
		if (dvs > dvd) {
			return 0;
		}

		int sign = 1;
		if (dividend < 0) { sign *= -1; }
		if (divisor < 0) { sign *= -1; }
		unsigned int absDvs = dvs;
		int step = 0;
		while (dvd > dvs) {
			dvs <<= 1;
			step++;
		}
		unsigned int ret = 0;
		while (dvd >= absDvs) {
			if (dvd >= dvs) {
				dvd -= dvs;
				ret += (unsigned int)(1 << step);
			}
			dvs >>= 1;
			step--;
		}
		if (-ret == (unsigned int)(INT_MIN) && sign == 1) {
		    return INT_MAX;
		}

		return ret * sign;
    }
};

#endif