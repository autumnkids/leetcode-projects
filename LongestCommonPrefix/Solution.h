#ifndef _SOLUTION_H_
#define _SOLUTION_H_

#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution {
public:
    string longestCommonPrefix(vector<string> &strs) {
		string result;
		bool needToBreak = false;

		if (strs.empty()) {
			return result;
		} else if (strs.size() == 1) {
			return strs[0];
		}

		string str0 = strs[0];
		for (int i = 0; i < str0.length(); i++) {
			// Loop the array
			for (int j = 1; j < strs.size(); j++) {
				// Once there is one string ends, break
				if (i >= strs[j].length()) {
					needToBreak = true;
					break;
				}

				if (str0[i] != strs[j][i]) {
					needToBreak = true;
					break;
				}
			}

			if (needToBreak) {
				break;
			} else {
				// Append to the result
				result.append(1, str0[i]);
			}
		}

		return result;
    }
};

#endif