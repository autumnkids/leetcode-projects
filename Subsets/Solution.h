#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <algorithm>
using namespace std;

class Solution {
public:
    vector<vector<int> > subsets(vector<int> &S) {
        vector<vector<int> > result;
		vector<int> output;
		sort(S.begin(), S.end());
		BackTracing(result, output, S, 0);
		return result;
    }

private:
	void BackTracing(vector<vector<int> > &result, vector<int> &output, vector<int> &S, int idx) {
		if (idx == S.size()) {
			result.push_back(output);
			return ;
		}

		int size = S.size();
		for (int i = idx; i < size; i++) {
			output.push_back(S[i]);
			BackTracing(result, output, S, i + 1);
			output.pop_back();
		}
		result.push_back(output);
	}
};

#endif