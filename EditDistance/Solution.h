#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
using namespace std;

class Solution {
public:
	// Note:
	//		- If A[i] == B[j], dp[i][j] = dp[i - 1][j - 1]
	//		- Else, dp[i][j] = min(dp[i - 1][j - 1], dp[i - 1][j], dp[i][j - 1]) + 1
	// ��������
    int minDistance(string word1, string word2) {
		int wlen1 = word1.length();
		int wlen2 = word2.length();
		if ((wlen1 == 0 && wlen2 == 0) || word1 == word2) {
			return 0;
		}
		if (wlen1 == 0) {
			return word2.length();
		}
		if (wlen2 == 0) {
			return word1.length();
		}

		int *dp1 = new int[wlen2 + 1];
		int *dp2 = new int[wlen2 + 1];
		for (int i = 0; i < wlen2 + 1; i++) {
			dp1[i] = 0;
			dp2[i] = i;
		}

		for (int i = 1; i < wlen1 + 1; i++) {
			dp1[0] = i;
			for (int j = 1; j < wlen2 + 1; j++) {
				if (word1[i - 1] == word2[j - 1]) {
					dp1[j] = dp2[j - 1];
				} else {
					dp1[j] = min(dp2[j], dp2[j - 1]);
					dp1[j] = min(dp1[j], dp1[j - 1]) + 1;
				}
			}
			int *tmp = dp1;
			dp1 = dp2;
			dp2 = tmp;
		}
		int ret = dp2[wlen2];
		delete [] dp1, dp2;

		return ret;
    }
};

#endif