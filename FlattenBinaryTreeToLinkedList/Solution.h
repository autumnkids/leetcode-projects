#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <stack>
#include <queue>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    void flatten(TreeNode *root) {
		if (root == NULL) {
			return ;
		}

        TreeNode *head = root;
		queue<TreeNode *> record;
		stack<TreeNode *> preOrder;
		preOrder.push(head);

		while (!preOrder.empty()) {
			TreeNode *top = preOrder.top();
			preOrder.pop();
			record.push(top);

			if (top->right) {
				preOrder.push(top->right);
			}
			if (top->left) {
				preOrder.push(top->left);
			}
		}

		TreeNode *prevNode = NULL;
		while (!record.empty()) {
			TreeNode *front = record.front();
			record.pop();

			if (prevNode != NULL) {
				prevNode->left = NULL;
				prevNode->right = front;
			}
			prevNode = front;
		}
    }
};

#endif