#ifndef _SOLUTION_H_
#define _SOLUTION_H_

#include <iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
		if (headA == NULL || headB == NULL) {
			return NULL;
		}

        ListNode *tmpA = headA;
		ListNode *tmpB = headB;
		int lenA = 0;
		int lenB = 0;

		while (tmpA != NULL) {
			lenA++;
			tmpA = tmpA->next;
		}

		while (tmpB != NULL) {
			lenB++;
			tmpB = tmpB->next;
		}

		tmpA = headA;
		tmpB = headB;
		int i = 0;
		if (lenA > lenB) {
			i = lenA - lenB;
			while (i) {
				tmpA = tmpA->next;
				i--;
			}
		} else if (lenB > lenA) {
			i = lenB - lenA;
			while (i) {
				tmpB = tmpB->next;
				i--;
			}
		}

		while (tmpA != NULL && tmpB != NULL) {
			if (tmpA == tmpB) {
				return tmpA;
			}

			tmpA = tmpA->next;
			tmpB = tmpB->next;
		}

		return NULL;
    }
};

#endif