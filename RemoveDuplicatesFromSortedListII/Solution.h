#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
	// Notes:
	//		- Delete each node while remove it
    ListNode *deleteDuplicates(ListNode *head) {
		if (head == NULL || head->next == NULL) {
			return head;
		}

		ListNode *prev = new ListNode(-1);
		prev->next = head;
		ListNode *cur = head;
		ListNode *next = head->next;
		head = prev;
		bool deleteCur = false;
		while (next != NULL) {
			if (next->val == cur->val) {
				ListNode *tmp = next;
				cur->next = tmp->next;
				next = tmp->next;
				delete tmp;
				deleteCur = true;
			} else {
				if (deleteCur) {
					deleteCur = false;
					prev->next = next;
					delete cur;
					cur = prev->next;
					next = cur->next;
				} else {
					prev = cur;
					cur = next;
					next = next->next;
				}
			}
		}
		if (deleteCur) {
			prev->next = next;
			delete cur;
		}

		// Remove dummy
		ListNode *dummy = head;
		head = head->next;
		delete dummy;

		return head;
    }
};

#endif