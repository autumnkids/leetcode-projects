#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

class Solution {
public:
    int search(int A[], int n, int target) {
		if (n == 0) {
			return -1;
		}

		return BinarySearch(A, 0, n - 1, target);
    }

private:
	// Note:
	//		If target < A[mid]:
	//			- Two situations, go to the first part:
	//				- A[start] <= target < A[mid] || pivot is between [A[start], A[mid]], so target is still in first part.
	//			- Else: in second part
	//		Else: (target > A[mid)
	//			- Two situations, go to the second part:
	//				- A[mid] < target <= A[end] || pivot is between [A[mid], A[end]], so target is still in second part.
	//			- Else: in first part
	int BinarySearch(int A[], int start, int end, int target) {
		if (start > end) {
			return -1;
		}

		int mid = (start + end) / 2;
		if (A[mid] == target) {
			return mid;
		}

		int ret = -1;
		if (target < A[mid]) {
			if (target >= A[start] || A[start] > A[mid]) {
				ret = BinarySearch(A, start, mid - 1, target);
			} else {
				ret = BinarySearch(A, mid + 1, end, target);
			}
		} else {
			if (target <= A[end] || A[mid] > A[end]) {
				ret = BinarySearch(A, mid + 1, end, target);
			} else {
				ret = BinarySearch(A, start, mid - 1, target);
			}
		}

		return ret;
	}
};

#endif