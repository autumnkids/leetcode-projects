#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

class Solution {
public:
    int findMin(vector<int> &num) {
		int nSize = num.size();
		if (nSize == 0) {
			return 0;
		}

		int min = num[0];
		for (int i = 1; i < nSize; i++) {
			if (num[i] > num[i - 1]) {
				continue;
			}
			if (num[i] < min) {
				min = num[i];
				break;
			}
		}

		return min;
    }
};

#endif