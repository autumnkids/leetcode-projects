#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <string>
#include <stack>
using namespace std;

const int moves[][2] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

class Solution {
public:
    bool exist(vector<vector<char> > &board, string word) {
		if (board.empty()) {
			return false;
		} else if (word.empty()) {
			return true;
		}

		m_rowSize = board.size();
		m_colSize = board[0].size();
		if (m_rowSize * m_colSize < word.length()) {
			return false;
		}
		m_visited = new bool*[m_rowSize];
		for (int i = 0; i < board.size(); i++) {
			m_visited[i] = new bool[m_colSize];
		}

		bool ret = false;
		for (int i = 0; i < m_rowSize; i++) {
			for (int j = 0; j < m_colSize; j++) {
				if (word[0] == board[i][j]) {
					ResetVisited();
					DFS(board, word, i, j, 0, ret);
					if (ret) {
						break;
					}
				}
			}
			if (ret) {
				break;
			}
		}

		return ret;
    }

private:
	struct Grid {
		int row;
		int col;
		int lvl;
		char ch;
	};

	bool **m_visited;
	int m_colSize;
	int m_rowSize;

	void ResetVisited() {
		for (int i = 0; i < m_rowSize; i++) {
			for (int j = 0; j < m_colSize; j++) {
				m_visited[i][j] = false;
			}
		}
	}

	void DFS(vector<vector<char> > &board, string &word, int row, int col, int lvl, bool &ret) {
		if (!ret) {
			if (lvl == word.length() - 1) {
				ret = true;
				return ;
			}

			m_visited[row][col] = true;
			for (int i = 0; i < 4; i++) {
				int nextRow = row + moves[i][0];
				int nextCol = col + moves[i][1];
				if (nextRow >=0 && nextRow < m_rowSize && nextCol >=0 && nextCol < m_colSize &&
					!m_visited[nextRow][nextCol] && lvl + 1 < word.length() && board[nextRow][nextCol] == word[lvl + 1]) {
					DFS(board, word, nextRow, nextCol, lvl + 1, ret);
				}
			}
			m_visited[row][col] = false;
		}
	}
};

#endif