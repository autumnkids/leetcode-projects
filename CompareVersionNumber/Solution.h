#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <string>
#include <iostream>
using namespace std;

class Solution {
public:
    int compareVersion(string version1, string version2) {
		int result = 0;
        int idx1 = FindVersionPoint(version1);
		idx1 = idx1 == -1 ? version1.length() : idx1;
		int idx2 = FindVersionPoint(version2);
		idx2 = idx2 == -1 ? version2.length() : idx2;

		string subStr1;
		subStr1.append(version1.begin(), version1.begin() + idx1);
		string subStr2;
		subStr2.append(version2.begin(), version2.begin() + idx2);

		int num1 = StringToInt(subStr1);
		int num2 = StringToInt(subStr2);
		if (num1 > num2) {
			result = 1;
		} else if (num1 < num2) {
			result = -1;
		} else if (idx1 + 1 >= version1.length() && idx2 + 1 >= version2.length()) {
			result = 0;
		} else {
			string rest1;
			if (idx1 + 1 < version1.length()) {
				rest1.append(version1.begin() + idx1 + 1, version1.end());
			}
			string rest2;
			if (idx2 + 1 < version2.length()) {
				rest2.append(version2.begin() + idx2 + 1, version2.end());
			}
			result = compareVersion(rest1, rest2);
		}

		return result;
    }

private:
	int FindVersionPoint(string version) {
		int len = version.length();
		int i = 0;
		while (i < len && version[i] != '.') {
			i++;
		}
		if (i == len) {
			i = -1;
		}

		return i;
	}

	int StringToInt(string str) {
		int result = 0;
		int len = str.length();
		for (int i = 0; i < len; i++) {
			result *= 10;
			result += str[i] - '0';
		}

		return result;
	}
};

#endif