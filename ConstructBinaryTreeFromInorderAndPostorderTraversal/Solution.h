#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    TreeNode *buildTree(vector<int> &inorder, vector<int> &postorder) {
		if (inorder.empty() || postorder.empty()) {
			return NULL;
		}

		TreeNode *root = new TreeNode(0);
		BuildTree(root, inorder, 0, inorder.size() - 1, postorder, 0, postorder.size() - 1);
		return root;
    }

private:
	void BuildTree(TreeNode *root, vector<int> &inorder, int istart, int iend, vector<int> &postorder, int pstart, int pend) {
		root->val = postorder[pend];

		// Find left tree
		int numLeft = istart;
		while (numLeft < inorder.size() && inorder[numLeft] != postorder[pend]) {
			numLeft++;
		}
		if (numLeft - istart >= 1) {
			root->left = new TreeNode(0);
			BuildTree(root->left, inorder, istart, numLeft - 1, postorder, pstart, pstart + numLeft - istart - 1);
		}
		if (iend - numLeft >= 1) {
			root->right = new TreeNode(0);
			BuildTree(root->right, inorder, numLeft + 1, iend, postorder, pstart + numLeft - istart, pend - 1);
		}
	}
};

#endif