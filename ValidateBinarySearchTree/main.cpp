#include <iostream>
#include "Solution.h"

int main() {
	Solution sol;
	TreeNode *root = new TreeNode(0);
	if (sol.isValidBST(root)) {
		cout << "True" << endl;
	}
	delete[] root;

	system("pause");
	return 0;
}