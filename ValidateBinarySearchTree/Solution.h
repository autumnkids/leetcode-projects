#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
#include <stack>
#include <limits>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
	// Or use Inorder Traversal, and judge if it's increasing
    bool isValidBST(TreeNode *root) {
		return VarifyBST(root, LONG_MIN, LONG_MAX);
    }

private:
	bool VarifyBST(TreeNode *root, long long rmin, long long lmax) {
		if (root == NULL) {
			return true;
		}

		if (root->val > rmin && root->val < lmax &&
			VarifyBST(root->left, rmin, root->val) &&
			VarifyBST(root->right, root->val, lmax)) {
			return true;
		}

		return false;
	}
};

#endif