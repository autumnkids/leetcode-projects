#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
using namespace std;

class Solution {
public:
    void setZeroes(vector<vector<int> > &matrix) {
		if (matrix.empty() || matrix[0].empty()) {
			return ;
		}

		bool firstRow = false; // True, if first row should be zeroes
		bool firstCol = false; // True, if first column should be zeroes
		int rSize = matrix.size();
		int cSize = matrix[0].size();
		for (int i = 0; i < rSize; i++) {
			if (matrix[i][0] == 0) {
				firstCol = true;
				break;
			}
		}
		for (int i = 0; i < cSize; i++) {
			if (matrix[0][i] == 0) {
				firstRow = true;
				break;
			}
		}

		for (int i = 1; i < rSize; i++) {
			for (int j = 1; j < cSize; j++) {
				if (matrix[i][j] == 0) {
					matrix[i][0] = 0;
					matrix[0][j] = 0;
				}
			}
		}

		// Set zeroes
		for (int i = 1; i < rSize; i++) {
			if (matrix[i][0] == 0) {
				for (int j = 1; j < cSize; j++) {
					matrix[i][j] = 0;
				}
			}
		}
		for (int j = 1; j < cSize; j++) {
			if (matrix[0][j] == 0) {
				for (int i = 1; i < rSize; i++) {
					matrix[i][j] = 0;
				}
			}
		}
		if (firstRow) {
			for (int j = 0; j < cSize; j++) {
				matrix[0][j] = 0;
			}
		}
		if (firstCol) {
			for (int i = 0; i < rSize; i++) {
				matrix[i][0] = 0;
			}
		}
    }
};

#endif