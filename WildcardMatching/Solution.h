#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
#include <string>
using namespace std;

class Solution {
public:
    bool isMatch(const char *s, const char *p) {
		if (s == NULL && p == NULL) {
			return true;
		}
		if (s == NULL || p == NULL) {
			return false;
		}

		string str(s);
		string ptr(p);
		int slen = str.length();
		int plen = ptr.length();
		int i = 0, j = 0;
		int star = -1;
		int smark = -1;
		while (i < slen) {
			if (str[i] == ptr[j] || ptr[j] == '?') {
				i++;
				j++;
			} else if (ptr[j] == '*') {
				star = j;
				j++;
				smark = i;
				if (j >= plen) {
					return true;
				}
			} else if (star >= 0) {
				j = star + 1;
				smark++;		// This is the point
				i = smark;
			} else {
				return false;
			}
		}

		while (j < plen) {
			if (ptr[j] != '*') {
				return false;
			}
			j++;
		}

		return true;
    }
};

#endif

/*

s: aabc
p: a*?b*

if (s[i] == p[j] || p[j] == '?') {
	i++;
	j++;
} else if (j - 1 >= 0 && p[j - 1] == '*') {
	i++;
} else if (star >= 0) {
	j = star + 1;
	i++;
} else if (p[j] == '*') {
	star = j;
	j++;
	if (j >= plen) {
		return true;
	}
}

*/