#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *partition(ListNode *head, int x) {
		if (head == NULL) {
			return NULL;
		}

		ListNode *dummy = new ListNode(-1);
		dummy->next = head;
		head = dummy;

		ListNode *target;
		target = new ListNode(x);
		ListNode *toBeDelete = target;
		ListNode *prev = head;
		ListNode *next = head->next;
		prev->next = target;
		target->next = next;
		next = target;

		while (next->next != NULL) {
			if (next->next->val >= target->val) {
				next = next->next;
			} else {
				ListNode *insert = next->next;
				prev->next = insert;
				next->next = insert->next;
				insert->next = target;
				prev = prev->next;
			}
		}

		prev->next = toBeDelete->next;
		delete toBeDelete;
		dummy = head;
		head = head->next;
		delete dummy;

		return head; 
    }
};

#endif