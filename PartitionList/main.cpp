#include "Solution.h"

int main() {
	Solution sol;
	ListNode *head = new ListNode(2);
	head->next = new ListNode(1);
	sol.partition(head, 1);

	return 0;
}