#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <algorithm>
using namespace std;

class Solution {
public:
    void nextPermutation(vector<int> &num) {
		if (num.empty()) {
			return ;
		}

		int nSize = num.size();
		int idx = nSize - 1;
		int alterIdx = 0;
		while (idx > 0 && num[idx] <= num[idx - 1]) {
			// Trace from the back and find the one starts to decrease from the back
			idx--;
		}
		if (idx > 0) {
			alterIdx = idx - 1;
		}

		// Sort the rest of parts after idx
		sort(num.begin() + idx, num.end());
		if (idx == 0) {
			return ;
		}

		while (idx < nSize) {
			if (num[idx] <= num[alterIdx]) {
				idx++;
			} else {
				int tmp = num[alterIdx];
				num[alterIdx] = num[idx];
				num[idx] = tmp;
				break;
			}
		}
    }
};

#endif