#ifndef _SOLUTION_H_
#define _SOLUTION_H_

#include <iostream>
#include <cmath>
#include <stack>

using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    bool isBalanced(TreeNode *root) {
		return AssignLevel(root);
    }

private:
	bool AssignLevel(TreeNode *root) {
		if (root == NULL) {
			return true;
		}

		stack<TreeNode *> tree;
		tree.push(root);
		TreeNode *prevNode = NULL;

		while(!tree.empty()) {
			TreeNode *curNode = tree.top();
			if (curNode->left == NULL && curNode->right == NULL) {
				curNode->val = 1;
				tree.pop();
			} else if (prevNode == NULL || prevNode->left == curNode || prevNode->right == curNode) {
				if (curNode->left) {
					tree.push(curNode->left);
				} else if (curNode->right) {
					tree.push(curNode->right);
				}
			} else if (curNode->left == prevNode) {
				if (curNode->right) {
					tree.push(curNode->right);
				}
			} else {
				if ((curNode->left == NULL && curNode->right != NULL && curNode->right->val != 1) || 
					(curNode->right == NULL && curNode->left != NULL && curNode->left->val != 1) ||
					(curNode->left != NULL && curNode->right != NULL && abs(curNode->left->val - curNode->right->val) > 1)) {
					return false;
				}

				// Assign level to root of the sub-tree
				if (curNode->left == NULL && curNode->right != NULL) {
					curNode->val = curNode->right->val + 1;
				} else if (curNode->left != NULL && curNode->right == NULL) {
					curNode->val = curNode->left->val + 1;
				} else {
					curNode->val = curNode->left->val > curNode->right->val ? curNode->left->val + 1 : curNode->right->val + 1;
				}

				tree.pop();
			}

			prevNode = curNode;
		}

		return true;
	}
};

#endif