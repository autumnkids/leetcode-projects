#ifndef _SOLUTION_H_
#define _SOLUTION_H_

#include <iostream>
#include <map>
using namespace std;

class Solution {
public:
	Solution() {
		landMarks['I'] = 1;
		landMarks['V'] = 5;
		landMarks['X'] = 10;
		landMarks['L'] = 50;
		landMarks['C'] = 100;
		landMarks['D'] = 500;
		landMarks['M'] = 1000;
	}

	virtual ~Solution() {
		landMarks.clear();
	}

    int romanToInt(string s) {
		int result = 0;
		int curValue = 0;
		int lastValue = 0;
		int len = s.length();

		for (int i = 0; i < len; i++) {
			curValue = landMarks[s[i]];
			if (i > 0 && curValue > lastValue) {
				curValue -= lastValue * 2;
			}
			result += curValue;

			lastValue = curValue;
		}

		return result;
    }

private:
	map<char, int> landMarks; // 1, 2, 3, 5, 10, 50, 100, 500, 1000
};

#endif