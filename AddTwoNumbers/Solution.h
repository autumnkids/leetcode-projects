#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
		if (l1 == NULL) {
			return l2;
		} else if (l2 == NULL) {
			return l1;
		}

		ListNode *result = new ListNode(0);
		ListNode *ret = result;
		ListNode *tmp1 = l1;
		ListNode *tmp2 = l2;
		int aheadDigit = 0;
		while (tmp1 != NULL && tmp2 != NULL) {
			int sum = tmp1->val + tmp2->val + aheadDigit;
			result->val = sum % 10;
			aheadDigit = sum / 10;

			tmp1 = tmp1->next;
			tmp2 = tmp2->next;
			if ((tmp1 || tmp2) || aheadDigit != 0) {
				result->next = new ListNode(0);
				result = result->next;
			}
		}

		if (tmp1) {
			while (tmp1) {
				int sum = tmp1->val + aheadDigit;
				result->val = sum % 10;
				aheadDigit = sum / 10;

				tmp1 = tmp1->next;
				if (tmp1 || aheadDigit != 0) {
					result->next = new ListNode(0);
					result = result->next;
				}
			}
			if (aheadDigit != 0) {
				result->val = aheadDigit;
				aheadDigit = aheadDigit / 10;
			}
		} else if (tmp2) {
			while (tmp2) {
				int sum = tmp2->val + aheadDigit;
				result->val = sum % 10;
				aheadDigit = sum / 10;

				tmp2 = tmp2->next;
				if (tmp2 || aheadDigit != 0) {
					result->next = new ListNode(aheadDigit);
					result = result->next;
				}
			}
			if (aheadDigit != 0) {
				result->val = aheadDigit;
				aheadDigit = aheadDigit / 10;
			}
		}

		if (aheadDigit != 0) {
			result->val = aheadDigit;
		}

		return ret;
    }
};

#endif