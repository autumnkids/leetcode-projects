#ifndef _SOLUTION_H
#define _SOLUTION_H

class Solution {
public:
    int trailingZeroes(int n) {
		// n! = 5^k * k! * a;
		if (n == 0) return 0;

		int sum = n / 5;
		sum += trailingZeroes(sum);
		return sum;
    }
};

#endif