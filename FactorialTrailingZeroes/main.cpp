#include <iostream>
#include "Solution.h"

using namespace std;

int main() {
	int n;
	Solution sol;
	while (cin >> n) {
		cout << sol.trailingZeroes(n) << endl;
	}
	return 0;
}