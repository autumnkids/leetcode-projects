#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <iostream>
using namespace std;

class Solution {
public:
    int removeDuplicates(int A[], int n) {
		if (n == 0) {
			return 0;
		}

		int i = 1, j = 1;
		while (i < n) {
			if (A[i] != A[j - 1]) {
				A[j] = A[i];
				j++;
			} else {
				// Find next not the same
				while (i < n && A[i] == A[j - 1]) {
					i++;
				}
				// Decrease by 1, since it will be added 1 after each round
				i--;
			}
			i++;
		}

		return j;
    }
};

#endif