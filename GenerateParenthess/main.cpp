#include <iostream>
#include "Solution.h"

int main() {
	Solution sol;
	vector<string> result = sol.generateParenthesis(3);
	for (int i = 0; i < result.size(); i++) {
		cout << result[i] << endl;
	}

	system("pause");
	return 0;
}