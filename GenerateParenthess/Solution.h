#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <string>
using namespace std;

class Solution {
public:
    vector<string> generateParenthesis(int n) {
        vector<string> result;
		if (n == 0) {
			result.push_back("");
			return result;
		}

		string output;
		BackTracing(0, 0, n, result, output);
		return result;
    }

private:
	void BackTracing(int left, int right, int n, vector<string> &result, string &output) {
		if (left == n && right == n) {
			result.push_back(output);
			return ;
		}

		if (left < n) {
			output.push_back('(');
			BackTracing(left + 1, right, n, result, output);
			output.pop_back();
		}
		if (left > right && right < n) {
			output.push_back(')');
			BackTracing(left, right + 1, n, result, output);
			output.pop_back();
		}
	}
};

#endif