#ifndef _SOLUTION_H
#define _SOLUTION_H

#include <vector>
#include <cmath>
using namespace std;

class Solution {
public:
	// Or math solution: result.push_back((i >> 1)^i);
    vector<int> grayCode(int n) {
		vector<int> result;
		result.push_back(0);
		for (int i = 0; i < n; i++) {
			int highestBit = 1 << i;
			int rSize = result.size();
			for (int j = rSize - 1; j >= 0; j--) {
				result.push_back(highestBit + result[j]);
			}
		}
		return result;
    }
};

#endif