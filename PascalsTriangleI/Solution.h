#ifndef _SOLUTION_H_
#define _SOLUTION_H_

#include <vector>
#include <iostream>
using namespace std;

class Solution {
public:
	virtual ~Solution() {
		delete [] m_record;
	}

	vector<vector<int> > generate(int numRows) {
		vector<vector<int> > result;

		for (int i = 0; i < numRows; i++) {
			vector<int> eachRow = getRow(i);
			result.push_back(eachRow);
		}

		return result;
    }

    vector<int> getRow(int rowIndex) {
		vector<int> result;

		if (rowIndex == 0) {
			result.push_back(1);
			return result;
		}

        InitializeRecord(rowIndex + 1);
		
		int prevValue = 0;
		for (int row = 2; row <= rowIndex; row++) {
			prevValue = m_record[0];
			for (int col = 1; col < rowIndex; col++) {
				int tmp = prevValue + m_record[col];;
				prevValue = m_record[col];
				m_record[col] = tmp;
			}
		}
		m_record[rowIndex] = 1;

		for (int i = 0; i <= rowIndex; i++) {
			result.push_back(m_record[i]);
		}

		return result;
    }

private:
	int *m_record;

	void InitializeRecord(int k) {
		// Ensure the k is >= 2
		m_record = new int[k];
		memset(m_record, 0, sizeof(int) * k);
		m_record[0] = 1;
		m_record[1] = 1;
	}
};

#endif